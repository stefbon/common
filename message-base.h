/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef GENERIC_MESSAGE_BASE_H
#define GENERIC_MESSAGE_BASE_H

#define NOTIFYFS_MESSAGE_TYPE_NOTSET           	0
#define NOTIFYFS_MESSAGE_TYPE_REGISTER		1
#define NOTIFYFS_MESSAGE_TYPE_SIGNOFF		2
#define NOTIFYFS_MESSAGE_TYPE_UPDATE		3
#define NOTIFYFS_MESSAGE_TYPE_SETWATCH		4
#define NOTIFYFS_MESSAGE_TYPE_DELWATCH		5
#define NOTIFYFS_MESSAGE_TYPE_CHANGEWATCH	6
#define NOTIFYFS_MESSAGE_TYPE_FSEVENT		7
#define NOTIFYFS_MESSAGE_TYPE_REMOVE		8
#define NOTIFYFS_MESSAGE_TYPE_VIEW		9

#define NOTIFYFS_MESSAGE_TYPE_REPLY		10

#define NOTIFYFS_MESSAGE_FSEVENT_FLAG_INDIR	1
#define NOTIFYFS_MESSAGE_FSEVENT_FLAG_ISDIR	2

struct notifyfs_register_message {
    uint64_t unique;
    unsigned char type;
    int mode;
    pid_t pid;
    pid_t tid;
};

struct notifyfs_signoff_message {
    uint64_t unique;
};

/*
    message to update a directory
    path is in buffer only if entry is not set
*/

struct notifyfs_update_message {
    uint64_t unique;
    int entry;
};

/*
    message to set a watch
    path is in buffer only if entry is not set
*/

struct notifyfs_setwatch_message {
    uint64_t unique;
    struct fseventmask_struct fseventmask;
    int view;
    int watch_id;
};

/*
    message to remove a watch
    from server to client when entry is deleted for example
    from client to server to explicit remove a watch
*/

struct notifyfs_delwatch_message {
    uint64_t unique;
    int watch_id;
};

/*
    message to change a watch
*/

struct notifyfs_changewatch_message {
    uint64_t unique;
    unsigned char action;
    int watch_id;
};

/*
    reply message to answer operation has been succesfull or not
*/

struct notifyfs_reply_message {
    uint64_t unique;
    int error;
};

/*
    message to send a fsevent to client
*/

struct notifyfs_fsevent_message {
    uint64_t unique;
    struct fseventmask_struct fseventmask;
    int entry;
    int watch_id;
    struct timespec detect_time;
    unsigned int flags;
};

/*
send a remove event, when no watch is found
this happens when for example a directory is removed
and every watch in it is removed

parent is index of parent entry of the removed entry
entry is index of the entry which is removed
inode is inode number of entry

path is in buffer 
name of entry is in buffer

both are seperated by a '\0'
if parent==entry name is empty

*/

struct notifyfs_remove_message {
    uint64_t unique;
    uint64_t ino;
    int parent;
    int entry;
    struct timespec detect_time;
};

/*
    send a view event, from client to server
    to inform the server a view is activated
*/

struct notifyfs_view_message {
    uint64_t unique;
    int view;
};

struct notifyfs_message_body {
    unsigned char type;
    union {
	struct notifyfs_register_message register_message;
	struct notifyfs_signoff_message signoff_message;
	struct notifyfs_update_message update_message;
	struct notifyfs_setwatch_message setwatch_message;
	struct notifyfs_delwatch_message delwatch_message;
	struct notifyfs_changewatch_message changewatch_message;
	struct notifyfs_reply_message reply_message;
	struct notifyfs_fsevent_message fsevent_message;
	struct notifyfs_remove_message remove_message;
	struct notifyfs_view_message view_message;
    } body;
};

#endif
