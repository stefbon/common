/*
 
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>
#include <time.h>

#define LOG_LOGAREA LOG_LOGAREA_SOCKET

#include "logging.h"
#include "beventloop-utils.h"
#include "notifyfs-fsevent.h"
// #include "notifyfs-io.h"
#include "message-base.h"
#include "message-receive.h"
#include "utils.h"

static struct notifyfs_message_cb_struct message_callbacks=NOTIFYFS_MESSAGE_CB_INIT;

/* assign the callback */

void assign_notifyfs_message_cb_register(void (*handle_register) (int fd, void *data, struct notifyfs_register_message *register_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_register=handle_register;
}

void assign_notifyfs_message_cb_signoff(void (*handle_signoff) (int fd, void *data, struct notifyfs_signoff_message *signoff_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_signoff=handle_signoff;
}

void assign_notifyfs_message_cb_update(void (*handle_update) (int fd, void *data, struct notifyfs_update_message *update_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_update=handle_update;
}

void assign_notifyfs_message_cb_setwatch(void (*handle_setwatch) (int fd, void *data, struct notifyfs_setwatch_message *setwatch_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_setwatch=handle_setwatch;
}

void assign_notifyfs_message_cb_delwatch(void (*handle_delwatch) (int fd, void *data, struct notifyfs_delwatch_message *delwatch_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_delwatch=handle_delwatch;
}

void assign_notifyfs_message_cb_reply(void (*handle_reply) (int fd, void *data, struct notifyfs_reply_message *reply_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_reply=handle_reply;
}

void assign_notifyfs_message_cb_fsevent(void (*handle_fsevent) (int fd, void *data, struct notifyfs_fsevent_message *fsevent_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_fsevent=handle_fsevent;
}

void assign_notifyfs_message_cb_remove(void (*handle_remove) (int fd, void *data, struct notifyfs_remove_message *remove_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_remove=handle_remove;
}

void assign_notifyfs_message_cb_view(void (*handle_view) (int fd, void *data, struct notifyfs_view_message *view_message, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_view=handle_view;
}

void assign_notifyfs_message_cb_default(void (*handle_default) (int fd, void *data, struct notifyfs_message_body *message_body, void *buff, int len, unsigned char typedata))
{
    message_callbacks.handle_default=handle_default;
}

/* function to receive a message, reacting on data on fd via callbacks*/
 
int receive_message(int fd, void *data, uint32_t events, unsigned char typedata, char *buffer, size_t lenbuffer)
{
    struct msghdr msg;
    struct iovec iov[2];
    ssize_t lenread;
    struct notifyfs_message_body message_body;

    if ( ! buffer) {

	logoutput("receive_message: error: buffer is not defined ");
	lenread=-EINVAL;
	goto out;

    }

    if ( events && EPOLLIN ) {

	logoutput("receive_message");

    } else {

	logoutput("receive_message: event %i not supported", events);
	lenread=-ENOTSUP;
	goto out;

    }

    /* a big lock around the recv buffer, this is because there is one buffer, better is to use different buffers... 
	one for each connection ....
    */

    readbuffer:

    /* prepare msg */

    iov[0].iov_base=(void *) &message_body;
    iov[0].iov_len=sizeof(struct notifyfs_message_body);

    iov[1].iov_base=(void *) buffer;
    iov[1].iov_len=lenbuffer;

    msg.msg_iov=iov;
    msg.msg_iovlen=2;

    msg.msg_control=NULL;
    msg.msg_controllen=0;

    msg.msg_name=NULL;
    msg.msg_namelen=0;

    lenread=recvmsg(fd, &msg, 0);

    logoutput("receive_message: msg_controllen %i msg_iovlen %i lenread %i", msg.msg_controllen, msg.msg_iovlen, (int)lenread);

    if ( lenread<=0 ){

	if (lenread<0) logoutput("receive_message: error %i recvmsg", errno);

    } else if ( msg.msg_controllen==0 ) {
	unsigned char message_caught=1;

	if (message_body.type==NOTIFYFS_MESSAGE_TYPE_REPLY) {

	    if (message_callbacks.handle_reply) {
		struct notifyfs_reply_message *reply_message;

		/* a reply */

		reply_message=(struct notifyfs_reply_message *) &(message_body.body.reply_message);

		(*message_callbacks.handle_reply) (fd, data, reply_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a reply message but handle reply function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_SIGNOFF) {

	    if (message_callbacks.handle_signoff) {
		struct notifyfs_signoff_message *signoff_message;

		/* a signoff*/

		signoff_message=(struct notifyfs_signoff_message *) &(message_body.body.signoff_message);

		(*message_callbacks.handle_signoff) (fd, data, signoff_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a signoff message but handle signoff function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_UPDATE) {

	    if (message_callbacks.handle_update) {
		struct notifyfs_update_message *update_message;

		/* an update (request to update the cache) */

		update_message=(struct notifyfs_update_message *) &(message_body.body.update_message);

		(*message_callbacks.handle_update) (fd, data, update_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a update message but handle update function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_SETWATCH) {

	    if (message_callbacks.handle_setwatch) {
		struct notifyfs_setwatch_message *setwatch_message;

		/* a setwatch (from client to notifyfs or from notifyfs to client fs) */

		setwatch_message=(struct notifyfs_setwatch_message *) &(message_body.body.setwatch_message);

		(*message_callbacks.handle_setwatch) (fd, data, setwatch_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a setwatch message but handle setwatch function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_DELWATCH) {

	    if (message_callbacks.handle_delwatch) {
		struct notifyfs_delwatch_message *delwatch_message;

		/* a delwatch (from client to notifyfs or from notifyfs to client fs) */

		delwatch_message=(struct notifyfs_delwatch_message *) &(message_body.body.delwatch_message);

		(*message_callbacks.handle_delwatch) (fd, data, delwatch_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a delwatch message but handle delwatch function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_REGISTER) {

	    if (message_callbacks.handle_register) {
		struct notifyfs_register_message *register_message;

		/* a register (from client to notifyfs) */

		register_message=(struct notifyfs_register_message *) &(message_body.body.register_message);

		(*message_callbacks.handle_register) (fd, data, register_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a register message but handle register function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_FSEVENT) {

	    if (message_callbacks.handle_fsevent) {
		struct notifyfs_fsevent_message *fsevent_message;

		/* a fsevent (from notifyfs to client or from remote notifyfs server to this notifyfs server */

		fsevent_message=(struct notifyfs_fsevent_message *) &(message_body.body.fsevent_message);

		(*message_callbacks.handle_fsevent) (fd, data, fsevent_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a fsevent message but handle fsevent function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_REMOVE) {

	    if (message_callbacks.handle_remove) {
		struct notifyfs_remove_message *remove_message;

		/* a raw remove from server to client */

		remove_message=(struct notifyfs_remove_message *) &(message_body.body.remove_message);

		(*message_callbacks.handle_remove) (fd, data, remove_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a remove message but handle remove function not defined");
		message_caught=0;

	    }

	} else if (message_body.type==NOTIFYFS_MESSAGE_TYPE_VIEW) {

	    if (message_callbacks.handle_view) {
		struct notifyfs_view_message *view_message;

		/* a view message (from client to notifyfs service) to inform the server a client is interested in a view */

		view_message=(struct notifyfs_view_message *) &(message_body.body.view_message);

		(*message_callbacks.handle_view) (fd, data, view_message, iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: received a view message but handle view function not defined");
		message_caught=0;

	    }

	} else {

	    message_caught=0;

	}

	if (message_caught==0) {

	    if (message_callbacks.handle_default) {

		(*message_callbacks.handle_default) (fd, data, &(message_body), iov[1].iov_base, iov[1].iov_len, typedata);

	    } else {

		logoutput("receive_message: no default function defined for unknown message");

	    }

	}

    }

    out:

    logoutput("receive_message: ready (lenread: %i)", lenread);

    return lenread;

}
