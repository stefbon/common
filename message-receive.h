/*
  2010, 2011 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef GENERIC_MESSAGE_RECEIVE_H
#define GENERIC_MESSAGE_RECEIVE_H

struct notifyfs_message_cb_struct {
    void (*handle_register) (int fd, void *data, struct notifyfs_register_message *register_message, void *buff, int len, unsigned char remote);
    void (*handle_signoff) (int fd, void *data, struct notifyfs_signoff_message *signoff_message, void *buff, int len, unsigned char remote);
    void (*handle_update) (int fd, void *data, struct notifyfs_update_message *update_message, void *buff, int len, unsigned char remote);
    void (*handle_setwatch) (int fd, void *data, struct notifyfs_setwatch_message *setwatch_message, void *buff, int len, unsigned char remote);
    void (*handle_changewatch) (int fd, void *data, struct notifyfs_changewatch_message *changewatch_message, void *buff, int len, unsigned char remote);
    void (*handle_delwatch) (int fd, void *data, struct notifyfs_delwatch_message *delwatch_message, void *buff, int len, unsigned char remote);
    void (*handle_reply) (int fd, void *data, struct notifyfs_reply_message *reply_message, void *buff, int len, unsigned char remote);
    void (*handle_fsevent) (int fd, void *data, struct notifyfs_fsevent_message *fsevent_message, void *buff, int len, unsigned char remote);
    void (*handle_remove) (int fd, void *data, struct notifyfs_remove_message *remove_message, void *buff, int len, unsigned char remote);
    void (*handle_view) (int fd, void *data, struct notifyfs_view_message *view_message, void *buff, int len, unsigned char remote);
    void (*handle_default) (int fd, void *data, struct notifyfs_message_body *message_body, void *buff, int len, unsigned char remote);
};

#define NOTIFYFS_MESSAGE_CB_INIT {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}

// Prototypes

void assign_notifyfs_message_cb_register(void (*handle_register) (int fd, void *data, struct notifyfs_register_message *register_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_signoff(void (*handle_signoff) (int fd, void *data, struct notifyfs_signoff_message *signoff_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_update(void (*handle_update) (int fd, void *data, struct notifyfs_update_message *update_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_setwatch(void (*handle_setwatch) (int fd, void *data, struct notifyfs_setwatch_message *setwatch_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_delwatch(void (*handle_delwatch) (int fd, void *data, struct notifyfs_delwatch_message *delwatch_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_reply(void (*handle_reply) (int fd, void *data, struct notifyfs_reply_message *reply_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_fsevent(void (*handle_fsevent) (int fd, void *data, struct notifyfs_fsevent_message *fsevent_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_remove(void (*handle_remove) (int fd, void *data, struct notifyfs_remove_message *remove_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_view(void (*handle_view) (int fd, void *data, struct notifyfs_view_message *view_message, void *buff, int len, unsigned char typedata));
void assign_notifyfs_message_cb_default(void (*handle_default) (int fd, void *data, struct notifyfs_message_body *message_body, void *buff, int len, unsigned char typedata));

int receive_message(int fd, void *data, uint32_t events, unsigned char typedata, char *buffer, size_t lenbuffer);

#endif
