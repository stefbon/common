/*

  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>

#define LOG_LOGAREA LOG_LOGAREA_MESSAGE

#include "logging.h"
#include "notifyfs-fsevent.h"
#include "message-base.h"
#include "message-send.h"
#include "utils.h"

uint64_t uniquectr=0;
pthread_mutex_t uniquectr_mutex=PTHREAD_MUTEX_INITIALIZER;

struct notifyfs_reply_struct {
    uint64_t unique;
    int error;
    struct timespec send_time;
    struct timespec reply_time;
    struct notifyfs_reply_struct *next;
    struct notifyfs_reply_struct *prev;
};

struct notifyfs_replyqueue_struct {
    struct notifyfs_reply_struct *first;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

static struct notifyfs_replyqueue_struct waiting_reply_queue={NULL, PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER};
static struct notifyfs_replyqueue_struct received_reply_queue={NULL, PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER};

struct notifyfs_reply_struct *create_notifyfs_reply(uint64_t unique, struct timespec *send_time)
{
    struct notifyfs_reply_struct *notifyfs_reply=NULL;

    notifyfs_reply=malloc(sizeof(struct notifyfs_reply_struct));

    if (notifyfs_reply) {

	notifyfs_reply->unique=unique;

	if (send_time) {

	    notifyfs_reply->send_time.tv_sec=send_time->tv_sec;
	    notifyfs_reply->send_time.tv_nsec=send_time->tv_nsec;

	} else {

	    get_current_time(&notifyfs_reply->send_time);

	}

	notifyfs_reply->reply_time.tv_sec=0;
	notifyfs_reply->reply_time.tv_nsec=0;

	notifyfs_reply->error=0;
	notifyfs_reply->next=NULL;
	notifyfs_reply->prev=NULL;

    }

    return notifyfs_reply;

}

void put_notifyfs_reply_queue(struct notifyfs_reply_struct *notifyfs_reply, struct notifyfs_replyqueue_struct *reply_queue)
{

    if (reply_queue->first) reply_queue->first->prev=notifyfs_reply;
    notifyfs_reply->next=reply_queue->first;
    reply_queue->first=notifyfs_reply;

}

void get_notifyfs_reply_queue(struct notifyfs_reply_struct *notifyfs_reply, struct notifyfs_replyqueue_struct *reply_queue)
{

    if (notifyfs_reply->next) notifyfs_reply->next->prev=notifyfs_reply->prev;
    if (notifyfs_reply->prev) notifyfs_reply->prev->next=notifyfs_reply->next;
    if (reply_queue->first==notifyfs_reply) reply_queue->first=notifyfs_reply->next;

}

struct notifyfs_reply_struct *lookup_notifyfs_reply(uint64_t unique, struct notifyfs_replyqueue_struct *reply_queue)
{
    struct notifyfs_reply_struct *notifyfs_reply=NULL;

    notifyfs_reply=reply_queue->first;

    while(notifyfs_reply) {

	if (notifyfs_reply->unique==unique) break;

	notifyfs_reply=notifyfs_reply->next;

    }

    return notifyfs_reply;

}

void init_notifyfs_reply(uint64_t unique)
{
    struct notifyfs_reply_struct *notifyfs_reply=create_notifyfs_reply(unique, NULL);

    if (notifyfs_reply) {

	pthread_mutex_lock(&waiting_reply_queue.mutex);

	put_notifyfs_reply_queue(notifyfs_reply, &waiting_reply_queue);

	pthread_mutex_unlock(&waiting_reply_queue.mutex);

    }

}

/* process the reply message 
    it's done by lookup of the reply in the queue
    if found send a broadcast
*/

void process_notifyfs_reply(uint64_t unique, int error)
{
    struct notifyfs_reply_struct *notifyfs_reply=NULL;

    pthread_mutex_lock(&waiting_reply_queue.mutex);

    notifyfs_reply=lookup_notifyfs_reply(unique, &waiting_reply_queue);

    if (notifyfs_reply) {

	notifyfs_reply->error=error;

	get_notifyfs_reply_queue(notifyfs_reply, &waiting_reply_queue);
	get_current_time(&notifyfs_reply->reply_time);

    }

    pthread_mutex_unlock(&waiting_reply_queue.mutex);

    if (notifyfs_reply) {

	pthread_mutex_lock(&received_reply_queue.mutex);

	put_notifyfs_reply_queue(notifyfs_reply, &received_reply_queue);

	pthread_cond_broadcast(&received_reply_queue.cond);

	pthread_mutex_unlock(&received_reply_queue.mutex);


    }

}

int wait_for_notifyfs_reply(uint64_t unique, unsigned char seconds)
{
    struct notifyfs_reply_struct *notifyfs_reply=NULL;
    struct timespec expire_time;
    int res=0;

    get_current_time(&expire_time);
    expire_time.tv_sec+=seconds;

    pthread_mutex_lock(&received_reply_queue.mutex);

    while(1) {

	notifyfs_reply=lookup_notifyfs_reply(unique, &received_reply_queue);

	if ( ! notifyfs_reply) {

	    res=pthread_cond_timedwait(&received_reply_queue.cond, &received_reply_queue.mutex, &expire_time);

	    if (res==ETIMEDOUT) {

		res=-ETIMEDOUT;
		break;

	    }

	} else {

	    break;

	}

    }

    if (notifyfs_reply) {

	get_notifyfs_reply_queue(notifyfs_reply, &received_reply_queue);
	res=abs(notifyfs_reply->error);

	logoutput("wait_for_notifyfs_reply: received a reply(unique: %li), send at %li.%li, received ar %li.%li", 
	(long int) unique, notifyfs_reply->send_time.tv_sec, notifyfs_reply->send_time.tv_nsec, notifyfs_reply->reply_time.tv_sec, notifyfs_reply->reply_time.tv_nsec);

	free(notifyfs_reply);

    }

    pthread_mutex_unlock(&received_reply_queue.mutex);

    return res;

}

uint64_t new_uniquectr()
{

    pthread_mutex_lock(&uniquectr_mutex);
    uniquectr++;
    pthread_mutex_unlock(&uniquectr_mutex);

    return uniquectr;
}

int send_register_message(int fd, uint64_t unique, pid_t pid, unsigned char type, void *buffer, size_t size)
{
    struct notifyfs_message_body message;

    message.type=NOTIFYFS_MESSAGE_TYPE_REGISTER;

    message.body.register_message.unique=unique;
    message.body.register_message.pid=pid;
    message.body.register_message.type=type;
    message.body.register_message.tid=message.body.register_message.pid;

    if (buffer) {

	return send_message(fd, &message, buffer, size);

    } else {
	char dummy[1];

	dummy[0]='\0';

	return send_message(fd, &message, (void *) dummy, 1);

    }



}

int send_reply_message(int fd, uint64_t unique, int error, void *buffer, size_t size)
{
    struct notifyfs_message_body message;

    message.type=NOTIFYFS_MESSAGE_TYPE_REPLY;

    message.body.reply_message.unique=unique;
    message.body.reply_message.error=error;

    if (buffer) {

	return send_message(fd, &message, buffer, size);

    } else {
	char dummy[1];

	dummy[0]='\0';

	return send_message(fd, &message, (void *) dummy, 1);

    }

}

int send_setwatch_message(int fd, uint64_t unique, unsigned long id, char *path, int view, int attrib_event, int xattr_event, int file_event, int move_event)
{
    struct notifyfs_message_body message;

    message.type=NOTIFYFS_MESSAGE_TYPE_SETWATCH;

    message.body.setwatch_message.watch_id=id;
    message.body.setwatch_message.unique=unique;

    message.body.setwatch_message.fseventmask.attrib_event=attrib_event;
    message.body.setwatch_message.fseventmask.xattr_event=xattr_event;
    message.body.setwatch_message.fseventmask.file_event=file_event;
    message.body.setwatch_message.fseventmask.move_event=move_event;
    message.body.setwatch_message.fseventmask.fs_event=0;

    message.body.setwatch_message.view=view;

    if (path) {

	return send_message(fd, &message, (void *) path, strlen(path)+1);

    } else {
	char dummy[1];

	dummy[0]='\0';

	return send_message(fd, &message, (void *) dummy, 1);

    }

}

int send_delwatch_message(int fd, uint64_t unique, unsigned long id)
{
    struct notifyfs_message_body message;
    char dummy[1];

    dummy[0]='\0';

    message.type=NOTIFYFS_MESSAGE_TYPE_DELWATCH;

    message.body.delwatch_message.unique=unique;
    message.body.delwatch_message.watch_id=id;

    return send_message(fd, &message, (void *) dummy, 1);

}

int send_changewatch_message(int fd, uint64_t unique, unsigned long id, unsigned char action)
{
    struct notifyfs_message_body message;
    char dummy[1];

    dummy[0]='\0';

    message.type=NOTIFYFS_MESSAGE_TYPE_CHANGEWATCH;

    message.body.changewatch_message.unique=unique;
    message.body.changewatch_message.watch_id=id;
    message.body.changewatch_message.action=action;

    return send_message(fd, &message, (void *) dummy, 1);

}

/*
    send a fsevent message local
    it's local cause the entry index is part of the message,
    and this is only usefull for processes sharing the entry cache
*/


int send_fsevent_message(int fd, uint64_t unique, unsigned long id, struct fseventmask_struct *fseventmask, int entryindex, unsigned int flags, struct timespec *detect_time)
{
    struct notifyfs_message_body message;
    char dummy[1];

    dummy[0]='\0';

    message.type=NOTIFYFS_MESSAGE_TYPE_FSEVENT;

    message.body.fsevent_message.unique=unique;

    replace_fseventmask(&message.body.fsevent_message.fseventmask, fseventmask);

    message.body.fsevent_message.entry=entryindex;

    message.body.fsevent_message.detect_time.tv_sec=detect_time->tv_sec;
    message.body.fsevent_message.detect_time.tv_nsec=detect_time->tv_nsec;
    message.body.fsevent_message.watch_id=id;
    message.body.fsevent_message.flags=flags;

    return send_message(fd, &message, (void *) dummy, 1);

}

/*
    send a fsevent to a remote server
    note the entry has no meaning,
    so a name is part of the message if there is an event on a entry in the directory
*/

int send_fsevent_message_remote(int fd, uint64_t unique, unsigned long id, struct fseventmask_struct *fseventmask, char *name, unsigned int flags, struct timespec *detect_time)
{
    struct notifyfs_message_body message;

    message.type=NOTIFYFS_MESSAGE_TYPE_FSEVENT;

    message.body.fsevent_message.unique=unique;

    replace_fseventmask(&message.body.fsevent_message.fseventmask, fseventmask);

    message.body.fsevent_message.entry=-1;

    message.body.fsevent_message.detect_time.tv_sec=detect_time->tv_sec;
    message.body.fsevent_message.detect_time.tv_nsec=detect_time->tv_nsec;
    message.body.fsevent_message.watch_id=id;
    message.body.fsevent_message.flags=flags;

    if (name) {

	return send_message(fd, &message, (void *) name, strlen(name)+1);

    } else {
	char dummy[1];

	dummy[0]='\0';

	return send_message(fd, &message, (void *) dummy, 1);

    }

}

int send_fsevent_message_remove(int fd, uint64_t unique, uint64_t ino, int parent, int entry, char *name, char *path)
{
    struct notifyfs_message_body message;
    int len0=strlen(path);

    message.type=NOTIFYFS_MESSAGE_TYPE_FSEVENT;

    message.body.remove_message.unique=unique;

    message.body.remove_message.ino=ino;
    message.body.remove_message.parent=parent;
    message.body.remove_message.entry=entry;

    if (name) {
	int len1=strlen(name);
	char buffer[len0 + 2 + len1];

	memcpy(buffer, path, len0);
	buffer[len0]='\0';
	memcpy(buffer+len0+1, name, len1);
	buffer[len0+1+len1]='\0';

	return send_message(fd, &message, (void *) buffer, len0+2+len1);

    } else {
	char buffer[len0 + 2];

	memcpy(buffer, path, len0);
	buffer[len0]='\0';
	buffer[len0+1]='\0';

	return send_message(fd, &message, (void *) buffer, len0+2);

    }

}



int send_view_message(int fd, uint64_t unique, int view)
{
    struct notifyfs_message_body message;
    char dummy[1];

    dummy[0]='\0';

    message.type=NOTIFYFS_MESSAGE_TYPE_VIEW;

    message.body.view_message.unique=unique;
    message.body.view_message.view=view;

    return send_message(fd, &message, dummy, 1);

}


int send_update_message(int fd, uint64_t unique, char *path)
{
    struct notifyfs_message_body message;

    message.type=NOTIFYFS_MESSAGE_TYPE_UPDATE;

    message.body.update_message.unique=unique;
    message.body.update_message.entry=-1;

    return send_message(fd, &message, (void *) path, strlen(path)+1);

}

/* function to send a raw message */

int send_message(int fd, struct notifyfs_message_body *message, void *data, int len)
{
    struct msghdr msg;
    struct iovec io_vector[2];
    int nreturn=0;

    msg.msg_controllen=0;
    msg.msg_control=NULL;

    msg.msg_name=NULL;
    msg.msg_namelen=0;

    io_vector[0].iov_base=(void *) message;
    io_vector[0].iov_len=sizeof(struct notifyfs_message_body);

    io_vector[1].iov_base=data;
    io_vector[1].iov_len=len;

    msg.msg_iov=io_vector;
    msg.msg_iovlen=2;

    /* the actual sending */

    nreturn=sendmsg(fd, &msg, 0);

    if ( nreturn==-1 ) nreturn=-errno;

    logoutput("send_message: return %i", nreturn);

    out:

    return nreturn;

}
