/*
  2010, 2011, 2012 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef GENERIC_MESSAGE_SEND_H
#define GENERIC_MESSAGE_SEND_H

// Prototypes

void init_notifyfs_reply(uint64_t unique);
void process_notifyfs_reply(uint64_t unique, int error);
int wait_for_notifyfs_reply(uint64_t unique, unsigned char seconds);

int send_register_message(int fd, uint64_t unique, pid_t pid, unsigned char type, void *buffer, size_t size);
int send_reply_message(int fd, uint64_t unique, int error, void *buffer, size_t size);
int send_setwatch_message(int fd, uint64_t unique, unsigned long id, char *path, int view, int attrib_event, int xattr_event, int file_event, int move_event);
int send_delwatch_message(int fd, uint64_t unique, unsigned long id);
int send_changewatch_message(int fd, uint64_t unique, unsigned long id, unsigned char action);
int send_fsevent_message(int fd, uint64_t unique, unsigned long id, struct fseventmask_struct *fseventmask, int entryindex, unsigned int flags, struct timespec *detect_time);
int send_fsevent_message_remote(int fd, uint64_t unique, unsigned long id, struct fseventmask_struct *fseventmask, char *name, unsigned int flags, struct timespec *detect_time);
int send_fsevent_message_remove(int fd, uint64_t unique, uint64_t ino, int parent, int entry, char *name, char *path);
int send_view_message(int fd, uint64_t unique, int view);
int send_update_message(int fd, uint64_t unique, char *path);

int send_message(int fd, struct notifyfs_message_body *message, void *data, int len);
uint64_t new_uniquectr();

#endif
