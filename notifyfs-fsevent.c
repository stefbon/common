/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/param.h>

#define LOG_LOGAREA LOG_LOGAREA_WATCHES

#include "logging.h"
#include "notifyfs-fsevent.h"

unsigned char compare_fseventmasks(struct fseventmask_struct *maska, struct fseventmask_struct *maskb)
{
    unsigned char differ=0;

    if (maska->cache_event != maskb->cache_event) {

	differ=1;
	goto out;

    }

    if (maska->attrib_event != maskb->attrib_event) {

	differ=1;
	goto out;

    }

    if (maska->xattr_event != maskb->xattr_event) {

	differ=1;
	goto out;

    }

    if (maska->file_event != maskb->file_event) {

	differ=1;
	goto out;

    }

    if (maska->move_event != maskb->move_event) {

	differ=1;
	goto out;

    }

    if (maska->fs_event != maskb->fs_event) {

	differ=1;

    }

    out:

    return differ;

}

/* merge two fsevent masks */

unsigned char merge_fseventmasks(struct fseventmask_struct *maska, struct fseventmask_struct *maskb)
{
    unsigned char differ=0;

    if (compare_fseventmasks(maska, maskb)==1) {
	struct fseventmask_struct maskc;

	maskc.cache_event = maska->cache_event | maskb->cache_event;
	maskc.attrib_event = maska->attrib_event | maskb->attrib_event;
	maskc.xattr_event = maska->xattr_event | maskb->xattr_event;
	maskc.file_event = maska->file_event | maskb->file_event;
	maskc.move_event = maska->move_event | maskb->move_event;
	maskc.fs_event = maska->fs_event | maskb->fs_event;

	if (compare_fseventmasks(maska, &maskc)==1) {

	    maska->cache_event = maskc.cache_event;
	    maska->attrib_event = maskc.attrib_event;
	    maska->xattr_event = maskc.xattr_event;
	    maska->file_event = maskc.file_event;
	    maska->move_event = maskc.move_event;
	    maska->fs_event = maskc.fs_event;

	    differ=1;

	}

    }

    return differ;

}

/* replace one fseventmask by another */

void replace_fseventmask(struct fseventmask_struct *maska, struct fseventmask_struct *maskb)
{

    maska->cache_event = maskb->cache_event;
    maska->attrib_event = maskb->attrib_event;
    maska->xattr_event = maskb->xattr_event;
    maska->file_event = maskb->file_event;
    maska->move_event = maskb->move_event;
    maska->fs_event = maskb->fs_event;

}

