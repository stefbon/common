/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef NOTIFYFS_FSEVENT_H
#define NOTIFYFS_FSEVENT_H

#define NOTIFYFS_RECVBUFFERSIZE					PATH_MAX + PATH_MAX

#define NOTIFYFS_ZERO_FSEVENTMASK				{0, 0, 0, 0, 0, 0}

#define NOTIFYFS_FSEVENT_STATUS_NONE				0
#define NOTIFYFS_FSEVENT_STATUS_WAITING				1
#define NOTIFYFS_FSEVENT_STATUS_PROCESSED			2
#define NOTIFYFS_FSEVENT_STATUS_DONE				3

#define NOTIFYFS_WATCHMASK_SELF					1
#define NOTIFYFS_WATCHMASK_CHILDS				2

#define NOTIFYFS_FSEVENT_NOTSET					0
#define NOTIFYFS_FSEVENT_ATTR					1
#define NOTIFYFS_FSEVENT_XATTR					2
#define NOTIFYFS_FSEVENT_FILE					4
#define NOTIFYFS_FSEVENT_MOVE					8
#define NOTIFYFS_FSEVENT_FS					16

#define NOTIFYFS_FSEVENT_CACHE_ADDED				1
#define NOTIFYFS_FSEVENT_CACHE_REMOVED				2
#define NOTIFYFS_FSEVENT_CACHE_SUBDIRS				4
#define NOTIFYFS_FSEVENT_CACHE_MIMETYPE				8

#define NOTIFYFS_FSEVENT_ATTRIB_NOTSET				1
#define NOTIFYFS_FSEVENT_ATTRIB_SELF				2
#define NOTIFYFS_FSEVENT_ATTRIB_CHILD				4
#define NOTIFYFS_FSEVENT_ATTRIB_MODE				8
#define NOTIFYFS_FSEVENT_ATTRIB_OWNER				16
#define NOTIFYFS_FSEVENT_ATTRIB_GROUP				32
#define NOTIFYFS_FSEVENT_ATTRIB_MODIFYTIME			64
#define NOTIFYFS_FSEVENT_ATTRIB_CHANGETIME			128
#define NOTIFYFS_FSEVENT_ATTRIB_CA				248

#define NOTIFYFS_FSEVENT_XATTR_NOTSET				1
#define NOTIFYFS_FSEVENT_XATTR_SELF				2
#define NOTIFYFS_FSEVENT_XATTR_CHILD				4
#define NOTIFYFS_FSEVENT_XATTR_CREATE				8
#define NOTIFYFS_FSEVENT_XATTR_MODIFY				16
#define NOTIFYFS_FSEVENT_XATTR_DELETE				32
#define NOTIFYFS_FSEVENT_XATTR_CA				56

#define NOTIFYFS_FSEVENT_FILE_NOTSET				1
#define NOTIFYFS_FSEVENT_FILE_SELF				2
#define NOTIFYFS_FSEVENT_FILE_CHILD				4
#define NOTIFYFS_FSEVENT_FILE_MODIFIED				8
#define NOTIFYFS_FSEVENT_FILE_SIZE				16
#define NOTIFYFS_FSEVENT_FILE_OPEN				32
#define NOTIFYFS_FSEVENT_FILE_READ				64
#define NOTIFYFS_FSEVENT_FILE_CLOSE_WRITE			128
#define NOTIFYFS_FSEVENT_FILE_CLOSE_NOWRITE			256
#define NOTIFYFS_FSEVENT_FILE_LOCK_ADD				512
#define NOTIFYFS_FSEVENT_FILE_LOCK_CHANGE			1024
#define NOTIFYFS_FSEVENT_FILE_LOCK_REMOVE			2048
#define NOTIFYFS_FSEVENT_FILE_LOCK_CA				3584
#define NOTIFYFS_FSEVENT_FILE_CA				4088

#define NOTIFYFS_FSEVENT_MOVE_NOTSET				1
#define NOTIFYFS_FSEVENT_MOVE_SELF				2
#define NOTIFYFS_FSEVENT_MOVE_CHILD				4
#define NOTIFYFS_FSEVENT_MOVE_CREATED				8
#define NOTIFYFS_FSEVENT_MOVE_MOVED				16
#define NOTIFYFS_FSEVENT_MOVE_MOVED_FROM			32
#define NOTIFYFS_FSEVENT_MOVE_MOVED_TO				64
#define NOTIFYFS_FSEVENT_MOVE_DELETED				128
#define NOTIFYFS_FSEVENT_MOVE_NLINKS				256
#define NOTIFYFS_FSEVENT_MOVE_CA				504

#define NOTIFYFS_FSEVENT_FS_NOTSET				1
#define NOTIFYFS_FSEVENT_FS_MOUNT				2
#define NOTIFYFS_FSEVENT_FS_UNMOUNT_REMOVE			4
#define NOTIFYFS_FSEVENT_FS_UNMOUNT_AUTOFS			8
#define NOTIFYFS_FSEVENT_FS_UNMOUNT				12

/*
    different types of local processes which connect to the server via the local socket
*/

#define NOTIFYFS_CLIENTTYPE_APP					1
#define NOTIFYFS_CLIENTTYPE_FUSEFS				2

/*
    owner type of a connection: what is the remote side

    - client: an application which connects to the local socket, sharing the inode/entry shared memory
	    the client has initiated the connection
    - server: a notfyfs server on another host
	    the server has initiated the connection
    - backend: a backend like a remote server, a fuse fs or the kernel
	    the backend has been connected to, so has not initiated it

*/

#define NOTIFYFS_OWNERTYPE_CLIENT				1
#define NOTIFYFS_OWNERTYPE_SERVER				2
#define NOTIFYFS_OWNERTYPE_BACKEND				3

/*
    different backends

    - server: a remote server, this server has made a connection to
    - fuse fs: it has initiated the connection. when it does that at first it looks like a client app,
	    but when it registers itself, the server changes this to backend
    - kernel: with certain filesystems it's the idea to connect to the kernel to forward the watch via netlink to the fs
	    (like cifs and nfs)
    - localhost: when (network) filesystems are served from the same host, it's a special case of server backend: no need to connect
*/

#define NOTIFYFS_BACKENDTYPE_SERVER				1
#define NOTIFYFS_BACKENDTYPE_FUSEFS				2
#define NOTIFYFS_BACKENDTYPE_KERNEL				3
#define NOTIFYFS_BACKENDTYPE_LOCALHOST				4

/*
    different types of the context of a fsevent
    - sync: caused by a sync
    - mount: is a mount or an unmount
    - default: anything else
*/


#define NOTIFYFS_FSEVENT_TYPE_DEFAULT			0
#define NOTIFYFS_FSEVENT_TYPE_SYNC			1
#define NOTIFYFS_FSEVENT_TYPE_MOUNT			2
#define NOTIFYFS_FSEVENT_TYPE_WATCH			3

/*
    different flags about the context of a fsevent
*/

#define NOTIFYFS_FSEVENT_FLAG_INDIR			1
#define NOTIFYFS_FSEVENT_FLAG_ISDIR			2
#define NOTIFYFS_FSEVENT_FLAG_NODIR			4

struct fseventmask_struct {
    int cache_event;
    int attrib_event;
    int xattr_event;
    int file_event;
    int move_event;
    int fs_event;
};

// Prototypes

unsigned char compare_fseventmasks(struct fseventmask_struct *maska, struct fseventmask_struct *maskb);
unsigned char merge_fseventmasks(struct fseventmask_struct *maska, struct fseventmask_struct *maskb);
void replace_fseventmask(struct fseventmask_struct *maska, struct fseventmask_struct *maskb);

#endif
