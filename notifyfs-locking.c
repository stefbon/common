/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"
#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"
#include "notifyfs-locking.h"

void increase_actors(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) {

	pthread_mutex_lock(&lock->mutex);

	while (lock->flags & 3) {

	    pthread_cond_wait(&lock->cond, &lock->mutex);

	}

	lock->flags+=4;

	pthread_mutex_unlock(&lock->mutex);

    }

}

void decrease_actors(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) {

	pthread_mutex_lock(&lock->mutex);

	lock->flags-=4;

	pthread_cond_broadcast(&lock->cond);
	pthread_mutex_unlock(&lock->mutex);

    }

}

int prepare_directory_exclusive(struct notifyfshm_directory_struct *directory, unsigned char locked)
{
    int lockset=-1;
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) {

	if (locked==0) pthread_mutex_lock(&lock->mutex);

	/*
	set the first bit to one, to announce that exclusive access to this directory is requested
	this prevents other threads to get access, but gives threads which have access at this moment
	the opportunity to leave

	the only situation which can prevent this thread to set this bit, is when this bit is already set

	*/

	if ((lock->flags & 1) == 0) {

	    lock->flags+=1;
	    lockset=0;

	}

	if (locked==0) pthread_mutex_unlock(&lock->mutex);

    }

    return lockset;

}

/*
    try to get an exclusive lock to do an insert or a remove
    goal is to wait for other threads to finish
    these threads by definition do only lookups
    since the directory is locked (lock & 1) to prevent
    other insert&remove threads to access
*/


void get_directory_exclusive(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) {

	pthread_mutex_lock(&lock->mutex);

	/* wait for the other actors to quit */

	while (lock->flags>>3 > 1) {

	    pthread_cond_wait(&lock->cond, &lock->mutex);

	}

	lock->flags+=2;

	pthread_mutex_unlock(&lock->mutex);

    }

}

void unlock_directory_exclusive(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) {

	pthread_mutex_lock(&lock->mutex);

	/* unset the first two bits */

	if (lock->flags & 1) lock->flags-=1;
	if (lock->flags & 2) lock->flags-=2;

	pthread_cond_broadcast(&lock->cond);
	pthread_mutex_unlock(&lock->mutex);

    }

}

void lock_directory(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) pthread_mutex_lock(&lock->mutex);

}

void unlock_directory(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) pthread_mutex_unlock(&lock->mutex);

}

void cond_wait_directory(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) pthread_cond_wait(&lock->cond, &lock->mutex);

}

void cond_broadcast_directory(struct notifyfshm_directory_struct *directory)
{
    struct notifyfshm_lock_struct *lock=NULL;

    if (directory->shared_lock==(unsigned short) -1) {

	lock=notifyfshm_malloc_lock();

	if (lock) directory->shared_lock=lock->index;

    } else {

	lock=get_lock(directory->shared_lock);

    }

    if (lock) pthread_cond_broadcast(&lock->cond);

}

