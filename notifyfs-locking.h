/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef _NOTIFYFS_LOCKING_H
#define _NOTIFYFS_LOCKING_H

/* prototypes */

void increase_actors(struct notifyfshm_directory_struct *directory);
void decrease_actors(struct notifyfshm_directory_struct *directory);

int prepare_directory_exclusive(struct notifyfshm_directory_struct *directory, unsigned char locked);
void get_directory_exclusive(struct notifyfshm_directory_struct *directory);
void unlock_directory_exclusive(struct notifyfshm_directory_struct *directory);

void lock_directory(struct notifyfshm_directory_struct *directory);
void unlock_directory(struct notifyfshm_directory_struct *directory);

void cond_wait_directory(struct notifyfshm_directory_struct *directory);
void cond_broadcast_directory(struct notifyfshm_directory_struct *directory);

#endif
