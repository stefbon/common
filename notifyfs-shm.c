/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>


#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"
#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"


static directory_table_t *directory_table[16];
static dirnode_table_t *dirnode_table[16];
static entry_table_t *entry_table[16];
static data_table_t *data_table[16];
static view_table_t *view_table[16];
static lock_table_t *lock_table[16];

static unsigned char initialized=0;

static int data_array_size=SIZE_ENTRY_HASHTABLE * ENTRY_NAMELEN_AVERAGE;

static struct shmchunks_struct directory_shmchunks={NULL, NULL, PTHREAD_MUTEX_INITIALIZER, 0};
static struct shmchunks_struct dirnode_shmchunks={NULL, NULL, PTHREAD_MUTEX_INITIALIZER, 0};
static struct shmchunks_struct entry_shmchunks={NULL, NULL, PTHREAD_MUTEX_INITIALIZER, 0};
static struct shmchunks_struct data_shmchunks={NULL, NULL, PTHREAD_MUTEX_INITIALIZER, 0};
static struct shmchunks_struct view_shmchunks={NULL, NULL, PTHREAD_MUTEX_INITIALIZER, 0};
static struct shmchunks_struct lock_shmchunks={NULL, NULL, PTHREAD_MUTEX_INITIALIZER, 0};

static void init_directory_array(struct notifyfshm_directory_struct directory_array[], int size, int nr)
{
    int i, base=nr*size;

    for (i=0;i<size;i++) {

	directory_array[i].index=base + i;

	directory_array[i].level=-1;
	directory_array[i].node=(unsigned short) - 1;

	directory_array[i].synctime.tv_sec=0;
	directory_array[i].synctime.tv_nsec=0;

	directory_array[i].first=(unsigned short) - 1;
	directory_array[i].last=(unsigned short) - 1 ;

	directory_array[i].shared_lock=(unsigned short) - 1;
	directory_array[i].count=0;
	directory_array[i].unsynced_count=0;

	directory_array[i].slctr=0;

    }

}

void init_dirnode(struct notifyfshm_dirnode_struct *dirnode)
{
    unsigned int j;

    dirnode->entry=0;
    dirnode->lock=0;

    for (j=0;j<NOTIFYFS_SKIPLIST_MAXLEVEL;j++) {

	dirnode->junction[j].lock=0;
	dirnode->junction[j].next=(unsigned short) - 1;
	dirnode->junction[j].prev=(unsigned short) - 1;
	dirnode->junction[j].count=0;

    }

    dirnode->type=NOTIFYFS_DIRNODE_TYPE_INIT;

}

static void init_dirnode_array(struct notifyfshm_dirnode_struct dirnode_array[], int size, int nr)
{
    int i, base=nr*size;

    for (i=0;i<size;i++) {

	dirnode_array[i].index=base + i;
	init_dirnode(&dirnode_array[i]);

    }

}

static void init_entry_array(struct notifyfshm_entry_struct entry_array[], int size, int nr)
{
    int i, base=nr*size;
    struct notifyfshm_entry_struct *entry=NULL;

    for (i=0;i<size;i++) {

	entry=&entry_array[i];

	memset(entry, 0, sizeof(struct notifyfshm_entry_struct));

	entry->index=base + i;

	entry->name=(unsigned short) - 1;
	entry->name_next=(unsigned short) - 1;
	entry->name_prev=(unsigned short) - 1;
	entry->parent=(unsigned short) - 1;
	entry->nameindex_value=0;

	entry->c_mode=0;
	entry->c_uid=0;
	entry->c_gid=0;
	entry->mount_unique=0;

	entry->c_mtim.tv_sec=0;
	entry->c_mtim.tv_nsec=0;

	entry->c_ctim.tv_sec=0;
	entry->c_ctim.tv_nsec=0;

	entry->type.c_filesize=0;

	entry->synctime.tv_sec=0;
	entry->synctime.tv_nsec=0;

    }

}

static void init_data_array(char data_array[], int size, int nr)
{

    memset(&data_array[0], '\0', size);

}

static void init_view_array(struct notifyfshm_view_struct view_array[], int size, int nr)
{
    int i, j, base=size*nr;

    for (i=0;i<size;i++) {

	view_array[i].index=base+i;
	view_array[i].status=0;
	view_array[i].pid=0;

	view_array[i].parent_entry=(unsigned short) - 1;

	view_array[i].mode=0;
	view_array[i].queue_index=0;
	view_array[i].client_queue_index=0;

	for (j=0;j<NOTIFYFS_VIEWQUEUE_LEN;j++) {

	    view_array[i].eventqueue[j].fseventmask.cache_event=0;
	    view_array[i].eventqueue[j].fseventmask.attrib_event=0;
	    view_array[i].eventqueue[j].fseventmask.xattr_event=0;
	    view_array[i].eventqueue[j].fseventmask.file_event=0;
	    view_array[i].eventqueue[j].fseventmask.move_event=0;
	    view_array[i].eventqueue[j].fseventmask.fs_event=0;

	    view_array[i].eventqueue[j].entry=(unsigned short) - 1;
	    view_array[i].eventqueue[j].row=0;

	}

	view_array[i].shared_lock=(unsigned short) - 1;

    }

}

static void init_lock_array(struct notifyfshm_lock_struct lock_array[], int size, int nr)
{
    int i, base=size*nr;

    for (i=0;i<size;i++) {

	lock_array[i].index=base+i;
	lock_array[i].flags=0;

    }

}

static struct shmchunk_struct *create_shmchunk(const char *name, size_t size, uid_t uid, gid_t gid, mode_t mode)
{
    struct shmchunk_struct *shmchunk=NULL;

    shmchunk=malloc(sizeof(struct shmchunk_struct));

    if (shmchunk) {

	shmchunk->size=0;
	shmchunk->fd=0;
	shmchunk->type=0;
	shmchunk->nr=0;
	shmchunk->next=NULL;
	shmchunk->address=NULL;

	memset(shmchunk->statefile, '\0', 32);

	if (strlen(name)>31) {

	    strncpy(shmchunk->statefile, name, 31);

	} else {

	    strcpy(shmchunk->statefile, name);

	}

	logoutput("create_shmchunk: calling shm_open");

	shmchunk->fd=shm_open(shmchunk->statefile, (O_CREAT | O_RDWR | O_TRUNC), mode);

	if (shmchunk->fd==-1) {

	    logoutput("create_shmchunk: error %i creating shared memory", errno);

	    free(shmchunk);
	    shmchunk=NULL;

	} else {

	    /* create a buffer */

	    ftruncate(shmchunk->fd, size);
	    fchown(shmchunk->fd, uid, gid);

	    logoutput("create_shmchunk: calling mmap");

	    shmchunk->address=mmap(0, size, (PROT_READ | PROT_WRITE), MAP_SHARED, shmchunk->fd, 0);

	    close(shmchunk->fd);
	    shmchunk->fd=0;

	    if (shmchunk->address==MAP_FAILED) {

		logoutput("create_shmchunk: calling mmap failed");

		shm_unlink(shmchunk->statefile);
		free(shmchunk);
		shmchunk=NULL;
		goto out;

	    }

	    shmchunk->size=size;

	    logoutput("create_shmchunk: calling mmap success");

	}

    }

    out:

    return shmchunk;

}

static void destroy_shmchunk(struct shmchunk_struct *shmchunk)
{
    if (!shmchunk) return;

    /* first unmap */

    if (shmchunk->address) {

	munmap(shmchunk->address, shmchunk->size);
	shmchunk->address=NULL;

    }

    shmchunk->size=0;

    /* close */

    if (shmchunk->fd>0) {

	close(shmchunk->fd);
	shmchunk->fd=0;

    }

    /* also remove it permanent ?? 
	this is not so obvious... */

    shm_unlink(shmchunk->statefile);
    free(shmchunk);

}

/* add a shmchunk to a list
    this list is ordered to shmchunk->nr

*/

static void add_shmchunk_to_list(struct shmchunk_struct *shmchunk, struct shmchunks_struct *shmchunks)
{

    pthread_mutex_lock(&shmchunks->mutex);

    if ( ! shmchunks->first) {

	shmchunks->first=shmchunk;
	shmchunks->last=shmchunk;

	shmchunks->nr=1;

    } else {
	struct shmchunk_struct *shmchunk_tmp=shmchunks->first;

	while(shmchunk_tmp) {

	    if (shmchunk->nr<shmchunk_tmp->nr) break;

	    shmchunk_tmp=shmchunk_tmp->next;

	}

	if (shmchunk_tmp) {

	    /* bigger shmchunk found */

	    if (shmchunk_tmp->prev) shmchunk_tmp->prev->next=shmchunk;
	    shmchunk->prev=shmchunk_tmp->prev;
	    shmchunk->next=shmchunk_tmp;
	    shmchunk_tmp->prev=shmchunk;

	    if (shmchunk_tmp==shmchunks->first) shmchunks->first=shmchunk;

	} else {

	    /* no bigger shmchunk found: add it at tail */

	    shmchunks->last->next=shmchunk;
	    shmchunks->last=shmchunk;
	    shmchunks->nr++;

	}

    }

    pthread_mutex_unlock(&shmchunks->mutex);

}

static struct shmchunk_struct *connect_shmchunk(const char *name, unsigned char type, int nr, unsigned char enablewrite)
{
    struct shmchunk_struct *shmchunk=NULL;

    shmchunk=malloc(sizeof(struct shmchunk_struct));

    if (shmchunk) {

	shmchunk->address=NULL;
	shmchunk->size=0;
	shmchunk->fd=0;
	shmchunk->type=type;
	shmchunk->nr=nr;
	shmchunk->next=NULL;

	memset(shmchunk->statefile, '\0', 32);

	if (strlen(name)>31) {

	    strncpy(shmchunk->statefile, name, 31);

	} else {

	    strcpy(shmchunk->statefile, name);

	}

	logoutput("connect_shmchunk: calling shm_open");

	if (enablewrite==0) {

	    shmchunk->fd=shm_open(shmchunk->statefile, O_RDONLY, 0);

	} else {

	    shmchunk->fd=shm_open(shmchunk->statefile, O_RDWR, 0);

	}

	if (shmchunk->fd==-1) {

	    logoutput("connect_shmchunk: error %i connect shared memory", errno);

	    free(shmchunk);
	    shmchunk=NULL;

	} else {
	    struct stat st;

	    /* map to the shm chunk */

	    if (fstat(shmchunk->fd, &st)==0) {

		logoutput("connect_shmchunk: got size %i, calling mmap", (int) st.st_size);

		/* use the size available */

		if (enablewrite==0) {

		    shmchunk->address=mmap(0, st.st_size, PROT_READ, MAP_SHARED, shmchunk->fd, 0);

		} else {

		    shmchunk->address=mmap(0, st.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, shmchunk->fd, 0);

		}

		close(shmchunk->fd);
		shmchunk->fd=0;

		if (shmchunk->address==MAP_FAILED) {

		    logoutput("connect_shmchunk: calling mmap failed");
		    shm_unlink(shmchunk->statefile);
		    free(shmchunk);
		    shmchunk=NULL;
		    goto out;

		}

		logoutput("connect_shmchunk: calling mmap success");

		shmchunk->size=st.st_size;

	    } else {

		close(shmchunk->fd);
		shmchunk->fd=0;
		shm_unlink(shmchunk->statefile);
		free(shmchunk);
		shmchunk=NULL;
		goto out;

	    }

	}

    }

    out:

    return shmchunk;

}

int initialize_sharedmemory_notifyfs(uid_t uid, gid_t gid)
{
    int nreturn=0, i;
    size_t size0, size1;
    struct shmchunk_struct *shmchunk=NULL;
    char name[32];

    if (initialized==0) {

	for (i=0; i<16; i++) {

	    directory_table[i]=NULL;
	    dirnode_table[i]=NULL;
	    entry_table[i]=NULL;
	    data_table[i]=NULL;
	    view_table[i]=NULL;

	}

	initialized=1;

    }

    /* the directory hash table */

    size0=sizeof(directory_table_t);

    logoutput("init_sharedmemory_notifyfs: creating directory table with size %i", size0);

    /* create a shm chunk */

    pthread_mutex_lock(&directory_shmchunks.mutex);
    snprintf(name, 32, "/notifyfs-directory-%i", directory_shmchunks.nr);

    directory_shmchunks.nr++;

    pthread_mutex_unlock(&directory_shmchunks.mutex);

    shmchunk=create_shmchunk(name, size0, 0, 0, (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));

    if (shmchunk) {

	logoutput("init_sharedmemory_notifyfs: created directory shm chunk");

	shmchunk->nr=0;
	shmchunk->type=SHMCHUNK_TYPE_DIRECTORY;

	shmchunk->table.directory_table=(directory_table_t *) (shmchunk->address);

	directory_table[0]=shmchunk->table.directory_table;

	directory_table[0]->index=0;
	directory_table[0]->size=SIZE_DIRECTORY_TABLE;

	init_directory_array(directory_table[0]->dir_array, SIZE_DIRECTORY_TABLE, 0);

	/* add to the inode shmchunks */

	pthread_mutex_lock(&dirnode_shmchunks.mutex);

	directory_shmchunks.first=shmchunk;
	directory_shmchunks.last=shmchunk;

	shmchunk->next=NULL;

	pthread_mutex_unlock(&dirnode_shmchunks.mutex);


    } else {

	logoutput("init_sharedmemory_notifyfs: unable to create the dir hash shm chunk");

	nreturn=-ENOMEM; /* good error code ? */
	goto error;

    }

    /* dir node */

    size1=sizeof(dirnode_table_t);

    logoutput("init_sharedmemory_notifyfs: creating dir node table with size %i", size1);

    pthread_mutex_lock(&dirnode_shmchunks.mutex);

    snprintf(name, 32, "/notifyfs-dirnode-%i", dirnode_shmchunks.nr);
    dirnode_shmchunks.nr++;

    pthread_mutex_unlock(&dirnode_shmchunks.mutex);

    shmchunk=create_shmchunk(name, size1, uid, gid, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH ));

    if (shmchunk) {

	logoutput("init_sharedmemory_notifyfs: created dir node shm chunk");

	shmchunk->nr=0;
	shmchunk->type=SHMCHUNK_TYPE_DIRNODE;

	shmchunk->table.dirnode_table=(dirnode_table_t *) (shmchunk->address);

	dirnode_table[0]=shmchunk->table.dirnode_table;
	dirnode_table[0]->index=0;
	dirnode_table[0]->size=SIZE_ENTRY_HASHTABLE;

	init_dirnode_array(dirnode_table[0]->dirnode_array, SIZE_ENTRY_HASHTABLE, 0);

	/* add to the inode shmchunks */

	pthread_mutex_lock(&dirnode_shmchunks.mutex);

	dirnode_shmchunks.first=shmchunk;
	dirnode_shmchunks.last=shmchunk;

	shmchunk->next=NULL;

	pthread_mutex_unlock(&dirnode_shmchunks.mutex);

    } else {

	logoutput("init_sharedmemory_notifyfs: unable to create the dir node shm chunk");

	nreturn=-ENOMEM; /* good error code ? */

	goto error;

    }

    /* entry table */

    size1=sizeof(entry_table_t);

    logoutput("init_sharedmemory_notifyfs: creating entry table with size %i", size1);

    pthread_mutex_lock(&entry_shmchunks.mutex);

    snprintf(name, 32, "/notifyfs-entry-%i", entry_shmchunks.nr);
    entry_shmchunks.nr++;

    pthread_mutex_unlock(&entry_shmchunks.mutex);

    shmchunk=create_shmchunk(name, size1, 0, 0, (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ));

    if (shmchunk) {

	logoutput("init_sharedmemory_notifyfs: created entry shm chunk");

	shmchunk->nr=0;
	shmchunk->type=SHMCHUNK_TYPE_ENTRY;

	shmchunk->table.entry_table=(entry_table_t *) (shmchunk->address);

	entry_table[0]=shmchunk->table.entry_table;
	entry_table[0]->index=0;
	entry_table[0]->size=SIZE_ENTRY_HASHTABLE;

	init_entry_array(entry_table[0]->entry_array, SIZE_ENTRY_HASHTABLE, 0);

	/* add to the entry shmchunks */

	pthread_mutex_lock(&entry_shmchunks.mutex);

	entry_shmchunks.first=shmchunk;
	entry_shmchunks.last=shmchunk;

	shmchunk->next=NULL;

	pthread_mutex_unlock(&entry_shmchunks.mutex);


    } else {

	logoutput("init_sharedmemory_notifyfs: unable to create the entry shm chunk");

	nreturn=-ENOMEM; /* good error code ? */

	goto error;

    }

    /* data table */

    size1=sizeof(data_table_t);

    logoutput("init_sharedmemory_notifyfs: creating data table with size %i", size1);

    pthread_mutex_lock(&data_shmchunks.mutex);

    snprintf(name, 32, "/notifyfs-data-%i", data_shmchunks.nr);
    data_shmchunks.nr++;

    pthread_mutex_unlock(&data_shmchunks.mutex);

    shmchunk=create_shmchunk(name, size1, 0, 0, (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ));

    if (shmchunk) {

	logoutput("init_sharedmemory_notifyfs: created data shm chunk");

	shmchunk->nr=0;
	shmchunk->type=SHMCHUNK_TYPE_DATA;

	shmchunk->table.data_table=(data_table_t *) (shmchunk->address);

	data_table[0]=shmchunk->table.data_table;
	data_table[0]->index=0;
	data_table[0]->size=SIZE_ENTRY_HASHTABLE * ENTRY_NAMELEN_AVERAGE;

	init_data_array(data_table[0]->data_array, data_table[0]->size, 0);

	/* add to the data shmchunks */

	pthread_mutex_lock(&data_shmchunks.mutex);

	data_shmchunks.first=shmchunk;
	data_shmchunks.last=shmchunk;

	shmchunk->next=NULL;

	pthread_mutex_unlock(&data_shmchunks.mutex);

    } else {

	logoutput("init_sharedmemory_notifyfs: unable to create the data shm chunk");

	nreturn=-ENOMEM; /* good error code ? */

	goto error;

    }

    /* view table */

    size1=sizeof(view_table_t);

    logoutput("init_sharedmemory_notifyfs: creating view table with size %i", size1);

    pthread_mutex_lock(&view_shmchunks.mutex);

    snprintf(name, 32, "/notifyfs-view-%i", view_shmchunks.nr);
    view_shmchunks.nr++;

    pthread_mutex_unlock(&view_shmchunks.mutex);

    shmchunk=create_shmchunk(name, size1, uid, gid, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH ));

    if (shmchunk) {

	logoutput("init_sharedmemory_notifyfs: created view shm chunk");

	shmchunk->nr=0;
	shmchunk->type=SHMCHUNK_TYPE_VIEW;

	shmchunk->table.view_table=(view_table_t *) (shmchunk->address);

	view_table[0]=shmchunk->table.view_table;
	view_table[0]->index=0;
	view_table[0]->size=SIZE_VIEW_TABLE;

	init_view_array(view_table[0]->view_array, view_table[0]->size, 0);

	/* add to the view shmchunks */

	pthread_mutex_lock(&view_shmchunks.mutex);

	view_shmchunks.first=shmchunk;
	view_shmchunks.last=shmchunk;

	shmchunk->next=NULL;

	pthread_mutex_unlock(&view_shmchunks.mutex);

    } else {

	logoutput("init_sharedmemory_notifyfs: unable to create the view shm chunk");

	nreturn=-ENOMEM; /* good error code ? */

	goto error;

    }

    /* lock table */

    size1=sizeof(lock_table_t);

    logoutput("init_sharedmemory_notifyfs: creating lock table with size %i", size1);

    pthread_mutex_lock(&lock_shmchunks.mutex);

    snprintf(name, 32, "/notifyfs-lock-%i", lock_shmchunks.nr);
    lock_shmchunks.nr++;

    pthread_mutex_unlock(&lock_shmchunks.mutex);

    shmchunk=create_shmchunk(name, size1, uid, gid, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));

    if (shmchunk) {

	logoutput("init_sharedmemory_notifyfs: created lock shm chunk");

	shmchunk->nr=0;
	shmchunk->type=SHMCHUNK_TYPE_LOCK;

	shmchunk->table.lock_table=(lock_table_t *) (shmchunk->address);

	lock_table[0]=shmchunk->table.lock_table;
	lock_table[0]->index=0;
	lock_table[0]->size=SIZE_LOCK_TABLE;

	init_lock_array(lock_table[0]->lock_array, lock_table[0]->size, 0);

	pthread_mutex_lock(&lock_shmchunks.mutex);

	lock_shmchunks.first=shmchunk;
	lock_shmchunks.last=shmchunk;

	shmchunk->next=NULL;

	pthread_mutex_unlock(&lock_shmchunks.mutex);

    } else {

	logoutput("init_sharedmemory_notifyfs: unable to create the lock shm chunk");

	nreturn=-ENOMEM; /* good error code ? */

	goto error;

    }

    return 0;

    error:

    return nreturn;

}


/* straight forward function to get access to the shared memory
    here it's in /dev/shm, and the name starts with notifyfs-..
*/

int connect_sharedmemory_notifyfs()
{
    int nreturn=0, i;
    DIR *dp=NULL;
    struct dirent *de;
    char *shmname_start="notifyfs-";
    int len=strlen(shmname_start), res;

    if (initialized==0) {

	for (i=0; i<16; i++) {

	    entry_table[i]=NULL;
	    data_table[i]=NULL;
	    view_table[i]=NULL;

	}

	initialized=1;

    }

    dp=opendir("/dev/shm");

    if (!dp) {

	nreturn=-errno;
	goto out;

    }

    while((de=readdir(dp))) {

	/* catch all direntries starting with a . */

	if (strncmp(de->d_name, ".", 1)==0) continue;
	if (!(strncmp(de->d_name, shmname_start, len)==0)) continue;

	if (strncmp(de->d_name, shmname_start, len)==0) {

	    if (strncmp(de->d_name+len, "directory-", 10)==0) {
		int nr;

		nr=atoi(de->d_name+len+10);

		if (! directory_table[nr]) {
		    struct shmchunk_struct *shmchunk=connect_shmchunk(de->d_name, SHMCHUNK_TYPE_DIRECTORY, 0, 0);

		    if (shmchunk) {

			logoutput("connect_sharedmemory_notifyfs: dirhash shm memory %s found: assign dirhash_table", de->d_name);

			/* table with hash table */

			shmchunk->table.directory_table=(directory_table_t *) (shmchunk->address);

			/* there can be only one hash table */

			directory_table[nr] = shmchunk->table.directory_table;

			add_shmchunk_to_list(shmchunk, &directory_shmchunks);

		    }

		}

	    } else if (strncmp(de->d_name+len, "dirnode-", 8)==0) {
		int nr;

		nr=atoi(de->d_name+len+8);

		if (! dirnode_table[nr]) {
		    struct shmchunk_struct *shmchunk=connect_shmchunk(de->d_name, SHMCHUNK_TYPE_DIRNODE, nr, 1);

		    if (shmchunk) {

			logoutput("connect_sharedmemory_notifyfs: dirnode shm memory %s found: assign dirnode_table", de->d_name);

			/* table with dirnode table */

			shmchunk->table.dirnode_table=(dirnode_table_t *) (shmchunk->address);
			dirnode_table[nr]=shmchunk->table.dirnode_table;

			add_shmchunk_to_list(shmchunk, &dirnode_shmchunks);

		    }

		}

	    } else if (strncmp(de->d_name+len, "entry-", 6)==0) {
		int nr;

		nr=atoi(de->d_name+len+6);

		if (! entry_table[nr]) {
		    struct shmchunk_struct *shmchunk=connect_shmchunk(de->d_name, SHMCHUNK_TYPE_ENTRY, nr, 0);

		    if (shmchunk) {

			logoutput("connect_sharedmemory_notifyfs: entry shm memory %s found: assign entry_table", de->d_name);

			/* table with entry table */

			shmchunk->table.entry_table=(entry_table_t *) (shmchunk->address);
			entry_table[nr]=shmchunk->table.entry_table;

			add_shmchunk_to_list(shmchunk, &entry_shmchunks);

		    }

		}

	    } else if (strncmp(de->d_name+len, "data-", 5)==0) {
		int nr;

		nr=atoi(de->d_name+len+5);

		if (! data_table[nr]) {
		    struct shmchunk_struct *shmchunk=connect_shmchunk(de->d_name, SHMCHUNK_TYPE_DATA, nr, 0);

		    if (shmchunk) {

			logoutput("connect_sharedmemory_notifyfs: data shm memory %s found: assign data_table", de->d_name);

			/* table with data */

			shmchunk->table.data_table=(data_table_t *) (shmchunk->address);
			data_table[nr]=shmchunk->table.data_table;

			add_shmchunk_to_list(shmchunk, &data_shmchunks);

		    }

		}

	    } else if (strncmp(de->d_name+len, "view-", 5)==0) {
		int nr;

		nr=atoi(de->d_name+len+5);

		if (! view_table[nr]) {
		    struct shmchunk_struct *shmchunk=connect_shmchunk(de->d_name, SHMCHUNK_TYPE_VIEW, nr, 1);

		    if (shmchunk) {

			logoutput("connect_sharedmemory_notifyfs: view shm memory %s found: assign view_table", de->d_name);

			/* table with view */

			shmchunk->table.view_table=(view_table_t *) (shmchunk->address);
			view_table[nr]=shmchunk->table.view_table;

			add_shmchunk_to_list(shmchunk, &view_shmchunks);

		    }

		}

	    } else if (strncmp(de->d_name+len, "lock-", 5)==0) {
		int nr;

		nr=atoi(de->d_name+len+6);

		if (! lock_table[nr]) {
		    struct shmchunk_struct *shmchunk=connect_shmchunk(de->d_name, SHMCHUNK_TYPE_LOCK, nr, 1);

		    if (shmchunk) {

			logoutput("connect_sharedmemory_notifyfs: lock shm memory %s found: assign lock_table", de->d_name);

			/* table with locks */

			shmchunk->table.lock_table=(lock_table_t *) (shmchunk->address);
			lock_table[nr]=shmchunk->table.lock_table;

			add_shmchunk_to_list(shmchunk, &lock_shmchunks);

		    }

		}

	    }

	}

    }

    closedir(dp);

    out:

    return nreturn;

}

struct notifyfshm_directory_struct *get_directory(unsigned short index)
{
    struct notifyfshm_directory_struct *directory=NULL;
    int ctr=0;

    if (index==(unsigned short) - 1) return NULL;

    ctr = index / SIZE_DIRECTORY_TABLE;

    pthread_mutex_lock(&directory_shmchunks.mutex);

    if (ctr<directory_shmchunks.nr) {
	int subindex=index%SIZE_DIRECTORY_TABLE;
	if (subindex<directory_table[ctr]->index) directory=&(directory_table[ctr]->dir_array[subindex]);

    }

    pthread_mutex_unlock(&directory_shmchunks.mutex);

    return directory;

}

struct notifyfshm_dirnode_struct *get_dirnode(unsigned short index)
{
    struct notifyfshm_dirnode_struct *dirnode=NULL;
    int ctr=0;

    if (index==(unsigned short) - 1) return NULL;

    ctr = index / SIZE_ENTRY_HASHTABLE;

    pthread_mutex_lock(&dirnode_shmchunks.mutex);

    if (ctr<dirnode_shmchunks.nr) {
	int subindex=index%SIZE_ENTRY_HASHTABLE;
	if (subindex<dirnode_table[ctr]->index) dirnode=&(dirnode_table[ctr]->dirnode_array[subindex]);

    }

    pthread_mutex_unlock(&dirnode_shmchunks.mutex);

    return dirnode;

}

struct notifyfshm_entry_struct *get_entry(unsigned short index)
{
    struct notifyfshm_entry_struct *entry=NULL;
    int ctr=0;

    if (index==(unsigned short) - 1) return NULL;

    ctr = index / SIZE_ENTRY_HASHTABLE;

    pthread_mutex_lock(&entry_shmchunks.mutex);

    if (ctr<entry_shmchunks.nr) {
	int subindex=index%SIZE_ENTRY_HASHTABLE;

	if (subindex<entry_table[ctr]->index) entry=&(entry_table[ctr]->entry_array[subindex]);

    }

    pthread_mutex_unlock(&entry_shmchunks.mutex);

    return entry;

}

char *get_data(unsigned short index)
{
    char *data=NULL;
    int ctr=0;

    if (index<0) return NULL;

    ctr = index / data_array_size;

    pthread_mutex_lock(&data_shmchunks.mutex);

    if (ctr<data_shmchunks.nr) {
	int subindex=index%data_array_size;

	if (subindex<data_table[ctr]->index) data=&(data_table[ctr]->data_array[subindex]);

    }

    pthread_mutex_unlock(&data_shmchunks.mutex);

    return data;

}

struct notifyfshm_view_struct *get_view(unsigned short index)
{
    struct notifyfshm_view_struct *view=NULL;
    int ctr=0;

    if (index<0) return NULL;

    ctr = index / SIZE_VIEW_TABLE;

    pthread_mutex_lock(&view_shmchunks.mutex);

    if (ctr<view_shmchunks.nr) {
	int subindex=index%SIZE_VIEW_TABLE;

	if (subindex<view_table[ctr]->index) view=&(view_table[ctr]->view_array[subindex]);

    }

    pthread_mutex_unlock(&view_shmchunks.mutex);

    return view;

}

struct notifyfshm_lock_struct *get_lock(unsigned short index)
{
    struct notifyfshm_lock_struct *lock=NULL;
    int ctr=0;

    if (index==(unsigned short) - 1) return NULL;

    ctr = index / SIZE_LOCK_TABLE;

    pthread_mutex_lock(&lock_shmchunks.mutex);

    if (ctr<lock_shmchunks.nr) {
	int subindex=index%SIZE_LOCK_TABLE;

	if (subindex<lock_table[ctr]->index) lock=&(lock_table[ctr]->lock_array[subindex]);

    }

    pthread_mutex_unlock(&lock_shmchunks.mutex);

    return lock;

}

void free_dirnode(struct notifyfshm_dirnode_struct *dirnode)
{
    unsigned char i;

    dirnode->entry=0;
    dirnode->lock=0;

    for (i=0;i<NOTIFYFS_SKIPLIST_MAXLEVEL;i++) {

	dirnode->junction[i].lock=0;
	dirnode->junction[i].next=0;
	dirnode->junction[i].prev=0;
	dirnode->junction[i].count=0;

    }

    dirnode->type=NOTIFYFS_DIRNODE_TYPE_FREE;

    /* here something like free dirnode -> set on list of free */

}

void free_directory(struct notifyfshm_directory_struct *directory)
{

    directory->count=0;
    directory->level=0;
    directory->node=0;
    directory->first=0;
    directory->last=0;

    if (directory->shared_lock>=0) {
	struct notifyfshm_lock_struct *lock=get_lock(directory->shared_lock);

	/* here "free" the lock */

	if (lock) {

	    pthread_mutex_destroy(&lock->mutex);
	    pthread_cond_destroy(&lock->cond);

	    /* here set the lock on an unused list */

	}

    }

    directory->synctime.tv_sec=0;
    directory->synctime.tv_nsec=0;

    /* here something like set on a list of free */

}

struct notifyfshm_directory_struct *notifyfshm_malloc_directory()
{
    struct notifyfshm_directory_struct *directory=NULL;

    pthread_mutex_lock(&directory_shmchunks.mutex);

    if (directory_shmchunks.last) {
	struct shmchunk_struct *shmchunk=directory_shmchunks.last;
	directory_table_t *directory_table=shmchunk->table.directory_table;

	if (directory_table->index + 1 < directory_table->size) {

	    /* get the next free dirnode */

	    directory=&(directory_table->dir_array[directory_table->index]);
	    directory_table->index++;

	} else {

	    /* here to do: add another shmchunk */

	    logoutput("notifyfs_malloc_directory: not enough space");

	}

    }

    pthread_mutex_unlock(&directory_shmchunks.mutex);

    return directory;

}

struct notifyfshm_dirnode_struct *notifyfshm_malloc_dirnode()
{
    struct notifyfshm_dirnode_struct *dirnode=NULL;

    pthread_mutex_lock(&dirnode_shmchunks.mutex);

    if (dirnode_shmchunks.last) {
	struct shmchunk_struct *shmchunk=dirnode_shmchunks.last;
	dirnode_table_t *dirnode_table=shmchunk->table.dirnode_table;

	if (dirnode_table->index + 1 < dirnode_table->size) {

	    /* get the next free dirnode */

	    dirnode=&(dirnode_table->dirnode_array[dirnode_table->index]);
	    dirnode_table->index++;

	} else {

	    /* here to do: add another shmchunk */

	    logoutput("notifyfs_malloc_dirnode: not enough space");

	}

    }

    pthread_mutex_unlock(&dirnode_shmchunks.mutex);

    return dirnode;

}

struct notifyfshm_entry_struct *notifyfshm_malloc_entry()
{
    struct notifyfshm_entry_struct *entry=NULL;

    pthread_mutex_lock(&entry_shmchunks.mutex);

    if (entry_shmchunks.last) {
	struct shmchunk_struct *shmchunk=entry_shmchunks.last;
	entry_table_t *entry_table=shmchunk->table.entry_table;

	if (entry_table->index + 1 < entry_table->size) {

	    /* get the next free entry */

	    entry=&(entry_table->entry_array[entry_table->index]);

	    entry_table->index++;

	} else {

	    /* add another shmchunk */

	    logoutput("notifyfs_malloc_entry: not enough space");

	}

    }

    pthread_mutex_unlock(&entry_shmchunks.mutex);

    return entry;

}

/* get the index in the array of free char space 
    it's possible that the array is split up in more than one pieces
    these pieces all have the same size

    here: when the space is not enough create a new shm chunk
*/

int notifyfshm_malloc_data(size_t size)
{
    int data=-1;

    pthread_mutex_lock(&data_shmchunks.mutex);

    if (data_shmchunks.last) {
	struct shmchunk_struct *shmchunk=data_shmchunks.last;
	data_table_t *data_table=shmchunk->table.data_table;

	if (data_table->index + 1 + size < data_table->size) {

	    data=data_table->index + data_table->size * shmchunk->nr;
	    data_table->index+=size;

	}

    }

    pthread_mutex_unlock(&data_shmchunks.mutex);

    return data;

}

struct notifyfshm_view_struct *notifyfshm_malloc_view()
{
    struct notifyfshm_view_struct *view=NULL;

    pthread_mutex_lock(&view_shmchunks.mutex);

    if (view_shmchunks.last) {
	struct shmchunk_struct *shmchunk=view_shmchunks.last;
	view_table_t *view_table=shmchunk->table.view_table;

	if (view_table->index + 1 < view_table->size) {

	    /* get the next free entry */

	    view=&(view_table->view_array[view_table->index]);

	    view_table->index++;

	} else {

	    /* add another shmchunk */

	    logoutput("notifyfs_malloc_view: not enough space");

	}

    }

    pthread_mutex_unlock(&view_shmchunks.mutex);

    return view;

}

struct notifyfshm_lock_struct *notifyfshm_malloc_lock()
{
    struct notifyfshm_lock_struct *lock=NULL;

    pthread_mutex_lock(&lock_shmchunks.mutex);

    if (lock_shmchunks.last) {
	struct shmchunk_struct *shmchunk=lock_shmchunks.last;
	lock_table_t *lock_table=shmchunk->table.lock_table;

	if (lock_table->index + 1 < lock_table->size) {
	    pthread_mutexattr_t mutexattr;
	    pthread_condattr_t condattr;

	    pthread_mutexattr_init(&mutexattr);
	    pthread_mutexattr_setpshared(&mutexattr, PTHREAD_PROCESS_SHARED); /* share with other processes */
	    pthread_mutexattr_setrobust(&mutexattr, PTHREAD_MUTEX_ROBUST); /* if process terminates, free the lock */

	    pthread_condattr_init(&condattr);
	    pthread_condattr_setpshared(&condattr, PTHREAD_PROCESS_SHARED);

	    /* get the next free lock */

	    lock=&(lock_table->lock_array[lock_table->index]);
	    lock_table->index++;

	    pthread_mutex_init(&lock->mutex, &mutexattr);
	    pthread_cond_init(&lock->cond, &condattr);

	} else {

	    /* add another shmchunk */

	    logoutput("notifyfs_malloc_lock: not enough space");

	}

    }

    pthread_mutex_unlock(&lock_shmchunks.mutex);

    return lock;

}

