/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef _NOTIFYFS_SHM_H
#define _NOTIFYFS_SHM_H

#include <fuse/fuse_lowlevel.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <pthread.h>

#define NOTIFYFS_INDEX_TYPE_NONE				0
#define NOTIFYFS_INDEX_TYPE_NAME				1

#define SHMCHUNK_TYPE_DIRECTORY					1
#define SHMCHUNK_TYPE_DIRNODE					2
#define SHMCHUNK_TYPE_ENTRY					3
#define SHMCHUNK_TYPE_DATA					4
#define SHMCHUNK_TYPE_ATTR					5
#define SHMCHUNK_TYPE_VIEW					6
#define SHMCHUNK_TYPE_LOCK					7

#define NOTIFYFS_VIEWQUEUE_LEN					64

#define NOTIFYFS_MOUNTMODE_ISBIND				1
#define NOTIFYFS_MOUNTMODE_ISROOT				2
#define NOTIFYFS_MOUNTMODE_AUTOFS				4
#define NOTIFYFS_MOUNTMODE_AUTOFS_INDIRECT			8
#define NOTIFYFS_MOUNTMODE_AUTOFS_MOUNTED			16
#define NOTIFYFS_MOUNTMODE_REMOUNT				32
#define NOTIFYFS_MOUNTMODE_FSTAB				64
#define NOTIFYFS_MOUNTMODE_BACKEND				128
#define NOTIFYFS_MOUNTMODE_CONNECTED				256
#define NOTIFYFS_MOUNTMODE_NOBACKEND				512

#define NOTIFYFS_MOUNTSTATUS_UP					1
#define NOTIFYFS_MOUNTSTATUS_DOWN				2
#define NOTIFYFS_MOUNTSTATUS_SLEEP				3
#define NOTIFYFS_MOUNTSTATUS_NOTUSED				4

#define NOTIFYFS_DIRNODE_TYPE_INIT				0
#define NOTIFYFS_DIRNODE_TYPE_BETWEEN				1
#define NOTIFYFS_DIRNODE_TYPE_START				2
#define NOTIFYFS_DIRNODE_TYPE_FREE				4

#define NOTIFYFS_VIEW_STATUS_NONE				0
#define NOTIFYFS_VIEW_STATUS_CLIENTDOWN				4

#define NOTIFYFS_VIEW_TYPE_INIT					0
#define NOTIFYFS_VIEW_TYPE_FREE					1


#ifndef SIZE_ENTRY_HASHTABLE
#define SIZE_ENTRY_HASHTABLE					32768
#endif

#ifndef SIZE_DIRECTORY_TABLE
#define SIZE_DIRECTORY_TABLE					2048
#endif

#define ENTRY_NAMELEN_AVERAGE					32

#define NOTIFYFS_SKIPLIST_PROB					4
#define NOTIFYFS_SKIPLIST_MAXLEVEL				4

#define NAMEINDEX_ROOT1						92			/* number of valid chars*/
#define NAMEINDEX_ROOT2						8464			/* 92 x 92 */
#define NAMEINDEX_ROOT3						778688			/* 92 x 92 x 92 */
#define NAMEINDEX_ROOT4						71639296		/* 92 ^ 4 */

#define SIZE_VIEW_TABLE						64			/* (max) number of views used by clients */

#define SIZE_LOCK_TABLE						SIZE_DIRECTORY_TABLE

struct notifyfshm_entry_struct {
    unsigned short 	index;
    unsigned short 	name;
    unsigned short 	name_next;
    unsigned short 	name_prev;
    unsigned short 	parent;
    unsigned int 	nameindex_value;
    mode_t		c_mode;
    uid_t		c_uid;
    gid_t		c_gid;
    unsigned long	mount_unique;
    union {
	off_t 		c_filesize;
	unsigned short 	directory;
    } type;
    struct timespec	c_mtim;
    struct timespec	c_ctim;
    struct timespec 	synctime;
};

struct eventqueue_struct {
    struct fseventmask_struct fseventmask;
    unsigned short 	entry;
    unsigned short 	row;
};

struct notifyfshm_view_struct {
    unsigned short 	index;
    pid_t 		pid;
    unsigned char 	status;
    unsigned short 	parent_entry;
    struct eventqueue_struct eventqueue[NOTIFYFS_VIEWQUEUE_LEN];
    unsigned short 	queue_index;
    unsigned short	client_queue_index;
    unsigned short	mode;
    unsigned short 	shared_lock;
};

struct notifyfshm_lock_struct {
    unsigned short	index;
    unsigned char 	flags;
    pthread_mutex_t 	mutex;
    pthread_cond_t 	cond;
};

struct node_junction_struct {
    unsigned char 	lock;
    unsigned short 	next;
    unsigned short 	prev;
    unsigned short 	count;
};

struct notifyfshm_dirnode_struct {
    unsigned short	index;
    unsigned short	entry;
    unsigned char 	lock;
    unsigned char 	type;
    struct node_junction_struct junction[NOTIFYFS_SKIPLIST_MAXLEVEL];
};

struct notifyfshm_directory_struct {
    unsigned short	index;
    signed char		level;
    unsigned short	node;
    unsigned short	first;
    unsigned short	last;
    unsigned short	shared_lock;
    unsigned short	count;
    unsigned short	unsynced_count;
    struct timespec 	synctime;
    unsigned long 	slctr;
};

typedef struct {
    unsigned short 			index;
    int 				size;
    struct notifyfshm_directory_struct 	dir_array[SIZE_DIRECTORY_TABLE];
} directory_table_t;

typedef struct {
    unsigned short 			index;
    int 				size;
    struct notifyfshm_dirnode_struct 	dirnode_array[SIZE_ENTRY_HASHTABLE];
} dirnode_table_t;

typedef struct {
    unsigned short 			index;
    int 				size;
    struct notifyfshm_entry_struct 	entry_array[SIZE_ENTRY_HASHTABLE];
} entry_table_t;

typedef struct {
    unsigned short 			index;
    int 				size;
    char 				data_array[SIZE_ENTRY_HASHTABLE * ENTRY_NAMELEN_AVERAGE];
} data_table_t;

typedef struct {
    unsigned short 			index;
    int 				size;
    struct notifyfshm_view_struct 	view_array[SIZE_VIEW_TABLE];
} view_table_t;

typedef struct {
    unsigned short 			index;
    int 				size;
    struct notifyfshm_lock_struct 	lock_array[SIZE_LOCK_TABLE];
} lock_table_t;

struct shmchunk_struct {
    char statefile[32];
    int fd;
    size_t size;
    unsigned char type;
    unsigned short nr;
    void *address;
    union {
	directory_table_t *directory_table;
	dirnode_table_t *dirnode_table;
	entry_table_t *entry_table;
	data_table_t *data_table;
	view_table_t *view_table;
	lock_table_t *lock_table;
    } table;
    struct shmchunk_struct *next;
    struct shmchunk_struct *prev;
};

struct shmchunks_struct {
    struct shmchunk_struct *first;
    struct shmchunk_struct *last;
    pthread_mutex_t mutex;
    unsigned short nr;
};


/* prototypes */

/* create and connect to shared memory */

int connect_sharedmemory_notifyfs();
int initialize_sharedmemory_notifyfs(uid_t t, gid_t gid);

/* get a shared object using it's index */

struct notifyfshm_directory_struct *get_directory(unsigned short index);
struct notifyfshm_dirnode_struct *get_dirnode(unsigned short index);
struct notifyfshm_entry_struct *get_entry(unsigned short index);
char *get_data(unsigned short index);
struct notifyfshm_view_struct *get_view(unsigned short index);
struct notifyfshm_lock_struct *get_lock(unsigned short index);

/* create a new object in shared memory */

struct notifyfshm_directory_struct *notifyfshm_malloc_directory();
struct notifyfshm_dirnode_struct *notifyfshm_malloc_dirnode();
struct notifyfshm_entry_struct *notifyfshm_malloc_entry();
int notifyfshm_malloc_data(size_t size);
struct notifyfshm_view_struct *notifyfshm_malloc_view();
struct notifyfshm_lock_struct *notifyfshm_malloc_lock();

void init_dirnode(struct notifyfshm_dirnode_struct *dirnode);
void free_dirnode(struct notifyfshm_dirnode_struct *dirnode);

void free_directory(struct notifyfshm_directory_struct *directory);

#endif
