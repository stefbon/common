/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"

#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"
#include "notifyfs-locking.h"
#include "notifyfs-sl.h"
#include "notifyfs-sl-delete.h"

static void unlock_dirnode_vector_deletefailed(struct vector_dirnode_struct *vector, struct notifyfshm_entry_struct *entry)
{
    unsigned char ctr=vector->minlevel;
    struct notifyfshm_dirnode_struct *dirnode=NULL;

    if (vector->direction==1) {

	while(ctr<=vector->maxlevel) {

	    dirnode=vector->lane[ctr].dirnode;

	    if (dirnode) {

		dirnode->junction[ctr].lock-=vector->lane[ctr].lockset;

		if (vector->lockset & NOTIFYFS_DIRNODE_RMLOCK) {
		    struct notifyfshm_dirnode_struct *next=get_dirnode(dirnode->junction[ctr].next);

		    if (next && next->entry==entry->index) {

			next->lock-=NOTIFYFS_DIRNODE_RMLOCK;
			vector->lockset-=NOTIFYFS_DIRNODE_RMLOCK;

		    }

		}

	    }

	    vector->lane[ctr].lockset=0;
	    ctr++;

	}

    } else if (vector->direction==-1) {

	while(ctr<=vector->maxlevel) {

	    dirnode=vector->lane[ctr].dirnode;

	    if (dirnode) {

		if (vector->lockset & NOTIFYFS_DIRNODE_RMLOCK) {

		    if (dirnode->entry==entry->index) {

			dirnode->lock-=NOTIFYFS_DIRNODE_RMLOCK;
			vector->lockset-=NOTIFYFS_DIRNODE_RMLOCK;

		    }

		}

		dirnode=get_dirnode(dirnode->junction[ctr].prev);

		if (dirnode) {

		    dirnode->junction[ctr].lock-=vector->lane[ctr].lockset;

		}

	    }

	    vector->lane[ctr].lockset=0;
	    ctr++;

	}

    }

}

/*
    function to remove a dirnode from a fast lane
    - dirnode, the dirnode in question
    - level, the number of the fast lane

    correct the next, prev and counters

    this function checks it's the last dirnode
*/

static void correct_dirnodes_sl(struct notifyfshm_dirnode_struct *dirnode, unsigned char level)
{
    struct notifyfshm_dirnode_struct *next, *prev;

    prev=get_dirnode(dirnode->junction[level].prev);
    next=get_dirnode(dirnode->junction[level].next);

    prev->junction[level].next=next->index;
    next->junction[level].prev=prev->index;

    prev->junction[level].count+=dirnode->junction[level].count - 1;

}

/*

    function to remove the dirnodes which are attached to a specific entry
    and correct the counts in the different fast lanes
    typically called when an entry is removed

*/

static void remove_dirnodes_sl(struct notifyfshm_directory_struct *directory, struct vector_dirnode_struct *vector, struct notifyfshm_entry_struct *entry)
{
    struct notifyfshm_dirnode_struct *dirnode=NULL;
    int level=vector->maxlevel;

    while(level>=0) {

	dirnode=vector->lane[level].dirnode;

	if (dirnode) {
	    struct notifyfshm_dirnode_struct *next=get_dirnode(dirnode->junction[level].next);

	    if (next->entry==entry->index) {

		/* next dirnode has to be removed also */

		while(level>=0) {

		    dirnode=vector->lane[level].dirnode;

		    if (level==directory->level) {

			/* test the prev (dirnode) is the same as next, in that case this has been the only dirnode left */

			if (dirnode->index==next->junction[level].next) directory->level--;

		    }

		    correct_dirnodes_sl(next, level);

		    next->junction[level].prev=(unsigned short) - 1;
		    next->junction[level].next=(unsigned short) - 1;
		    next->junction[level].count=0;
		    next->junction[level].lock=0;

		    dirnode->junction[level].lock-=vector->lane[level].lockset;

		    vector->lane[level].lockset=0;
		    vector->lane[level].dirnode=NULL;

		    level--;

		}

		free_dirnode(next);

		break;

	    } else {

		/* entry is in between dirnodes in this lane: no dirnode points to entry */

		dirnode->junction[level].count--;

		dirnode->junction[level].lock-=vector->lane[level].lockset;
		vector->lane[level].lockset=0;
		vector->lane[level].dirnode=NULL;

	    }

	}

	level--;

    }

}

static void delete_entry_nonempty_sl(struct notifyfshm_directory_struct *directory, struct notifyfshm_entry_struct *entry, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_entry_struct *search_entry=NULL;
    struct vector_dirnode_struct vector;
    int difference;
    const char *name=get_data(entry->name);
    unsigned int lenname=strlen(name), nameindex_value=entry->nameindex_value;
    unsigned char exclusive=0, entryfound=0;
    unsigned int search_row=0;

    increase_actors(directory);

    if (directory->count==0) goto unlock;

    init_vector_lane(&vector);

    search_entry=get_entry(directory->first);
    search_row=1;
    *error=ENOENT;

    /*
	when there are fastlanes, take them first
    */

    if (directory->level>=0) {
	struct notifyfshm_dirnode_struct *dirnode, *next_dirnode;
	int level;
	struct notifyfshm_entry_struct *next_entry=NULL;

	/* head of the skiplist */

	level=directory->level;

	vector.maxlevel=level;
	vector.lane[level].dirnode=get_dirnode(directory->node);
	vector.direction=-1;

	while(level>=0) {

	    lock_directory(directory);

	    dirnode=vector.lane[level].dirnode;
	    next_dirnode=get_dirnode(dirnode->junction[level].next);

	    difference=1;

	    if (next_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		next_entry=get_entry(next_dirnode->entry);
		difference=compare_entry(next_entry, name, nameindex_value, lenname);

	    }

	    if (difference>=0) {

		if (difference==0) {

		    if (entryfound==0) {

			/* rm lock already set ?*/

			if (next_dirnode->lock & NOTIFYFS_DIRNODE_RMLOCK) {

			    *error=EAGAIN;
			    vector.minlevel=level+1;
			    unlock_directory(directory);
			    goto out;

			}

			search_entry=next_entry;
			search_row+=dirnode->junction[level].count - 1;
			*error=0;
			entryfound=1;

		    }

		    if (level==directory->level) {

			if (dirnode->entry<0 && next_dirnode->junction[level].next==next_dirnode->junction[level].prev) {

			    /* this dirnode will be deleted and it's the only one in this lane */
			    /* try to lock the directory exclusive */

			    if (prepare_directory_exclusive(directory, 1)==-1) {

				*error=EAGAIN;
				vector.minlevel=level+1;
				unlock_directory(directory);
				goto out;

			    }

			    exclusive=1;

			}

		    }

		}

		/* here try to lock the region in this lane */

		if (next_dirnode->lock & NOTIFYFS_DIRNODE_RMLOCK) {

		    *error=EAGAIN;
		    vector.minlevel=level+1;
		    unlock_directory(directory);
		    goto out;

		} else if (dirnode->junction[level].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

		    *error=EAGAIN;
		    vector.minlevel=level+1;
		    unlock_directory(directory);
		    goto out;

		} else if (dirnode->junction[level].lock>>3 > 0) {

		    dirnode->junction[level].lock|=NOTIFYFS_DIRNODE_BLOCKLOCK;
		    vector.lane[level].lockset|=NOTIFYFS_DIRNODE_BLOCKLOCK;
		    vector.lockset|=NOTIFYFS_DIRNODE_BLOCKLOCK;

		    while(dirnode->junction[level].lock>>3 > 0) {

			cond_wait_directory(directory);

		    }

		}

		dirnode->junction[level].lock+=NOTIFYFS_DIRNODE_WRITELOCK;
		vector.lane[level].lockset+=NOTIFYFS_DIRNODE_WRITELOCK;
		vector.lockset|=NOTIFYFS_DIRNODE_WRITELOCK;

		/* set the rmlock: howto unset it when remove is not possible */

		if (difference==0 && ! (vector.lockset & NOTIFYFS_DIRNODE_RMLOCK)) {

		    next_dirnode->lock=NOTIFYFS_DIRNODE_RMLOCK;
		    vector.lockset|=NOTIFYFS_DIRNODE_RMLOCK;

		}

		level--;

		if (level>=0) vector.lane[level].dirnode=dirnode;

	    } else {

		/* step */

		vector.lane[level].step+=dirnode->junction[level].count;
		vector.lane[level].dirnode=next_dirnode;

		if (next_entry->name_next==(unsigned short) - 1) {

		    search_entry=next_entry;
		    search_row+=dirnode->junction[level].count - 1;

		} else {

		    search_entry=get_entry(next_entry->name_next);
		    search_row+=dirnode->junction[level].count;

		}

	    }

	    unlock_directory(directory);

	}

    }

    /*
	when here, the algoritm has reached the lowest level, and that is the
	linked list of entries
	check first there is not already an exact match found..
	what counts is that entry->name <= name
    */

    if (search_entry!=entry) {

	while (search_entry) {

	    difference=compare_entry(search_entry, name, nameindex_value, lenname);

	    if (difference<0) {

		/* before name still */

		search_entry=get_entry(search_entry->name_next);
		search_row++;

	    } else if (difference==0) {

		/* exact match */

		*error=0;
		break;

	    } else {

		/* past name: not found */

		search_entry=NULL;
		break;

	    }

	}

    }

    if (search_entry==entry) {

	*error=0;

	if (exclusive==1) {

	    get_directory_exclusive(directory);

	} else {

	    lock_directory(directory);

	}

	/* remove the different dirnodes */

	remove_dirnodes_sl(directory, &vector, entry);

	/* remove from the linked list */

	if (entry->name_next==(unsigned short) - 1) {

	    /* no next: must be the latest */

	    if (directory->last==entry->index) {

		directory->last=entry->name_prev;

	    } else {

		logoutput("delete_entry_nonempty_sl: internal error, latest entry (%i) differs from directory latest (%i)", entry->index, directory->last);

	    }

	} else {
	    struct notifyfshm_entry_struct *next_entry=NULL;

	    next_entry=get_entry(entry->name_next);
	    next_entry->name_prev=entry->name_prev;

	}

	if (entry->name_prev==(unsigned short) - 1) {

	    /* no prev: must be the first */

	    if (directory->first==entry->index) {

		directory->first=entry->name_next;

	    } else {

		logoutput("delete_entry_nonempty_sl: internal error, first entry (%i) differs from directory first (%i)", entry->index, directory->first);

	    }

	} else {
	    struct notifyfshm_entry_struct *prev_entry=NULL;

	    prev_entry=get_entry(entry->name_prev);
	    prev_entry->name_next=entry->name_next;

	}

	directory->count--;

	if (exclusive==1) {

	    unlock_directory_exclusive(directory);
	    exclusive=0;

	} else {

	    cond_broadcast_directory(directory);
	    unlock_directory(directory);

	}

    }

    out:

    if (vector.lockset>0) {

	/* release the locks set on the fast lanes */

	lock_directory(directory);

	if (*error==0) {

	    unlock_dirnode_vector(&vector, directory);

	} else {

	    if (entry) {

		unlock_dirnode_vector_deletefailed(&vector, entry);

	    } else {

		unlock_dirnode_vector(&vector, directory);

	    }

	}

	cond_broadcast_directory(directory);
	unlock_directory(directory);

    }

    unlock:

    if (exclusive==1) unlock_directory_exclusive(directory);

    decrease_actors(directory);

    if (row) *row=search_row;

}

void delete_entry_sl(struct notifyfshm_entry_struct *entry, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_entry_struct *parent=get_entry(entry->parent);
    struct notifyfshm_directory_struct *directory=NULL;

    directory=get_directory_sl(parent, 0, error);

    if (directory) {
	unsigned int count=0;

	while(count<10) {

	    delete_entry_nonempty_sl(directory, entry, row, error);

	    if (*error==0) {

		break;

	    } else if (*error==EAGAIN) {

		logoutput("delete_entry_sl: operation blocked, try again");

	    } else {

		logoutput("delete_entry_sl: error %i", *error);
		break;

	    }

	    count++;

	}

    } else {

	/* no directory found */

	logoutput("delete_entry_sl: no directory");

    }

}

void delete_entry_nonempty_sl_batch(struct notifyfshm_directory_struct *directory, struct notifyfshm_entry_struct *entry, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_entry_struct *search_entry=NULL;
    struct vector_dirnode_struct vector;
    int difference;
    const char *name=get_data(entry->name);
    unsigned int lenname=strlen(name), nameindex_value=entry->nameindex_value;
    int ctr=0;
    unsigned int search_row=0;
    unsigned char entryfound=0;

    init_vector_lane(&vector);

    search_entry=get_entry(directory->first);
    search_row=1;
    *error=ENOENT;

    /*
	when there are fastlanes, take them first
    */

    if (directory->level>=0) {
	struct notifyfshm_dirnode_struct *dirnode, *next_dirnode;
	int level;
	struct notifyfshm_entry_struct *next_entry=NULL;

	/* head of the skiplist */

	level=directory->level;

	vector.maxlevel=level;
	vector.lane[level].dirnode=get_dirnode(directory->node);
	vector.direction=1;

	while(level>=0) {

	    dirnode=vector.lane[level].dirnode;
	    next_dirnode=get_dirnode(dirnode->junction[level].next);

	    difference=1;

	    if (next_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		next_entry=get_entry(next_dirnode->entry);
		difference=compare_entry(next_entry, name, nameindex_value, lenname);

	    }

	    if (difference>=0) {

		if (difference==0) {

		    if (entryfound==0) {

			search_entry=next_entry;
			ctr+=dirnode->junction[level].count;
			search_row+=dirnode->junction[level].count;
			*error=0;

		    }

		}

		level--;
		if (level>=0) vector.lane[level].dirnode=dirnode;

	    } else {

		/* step */

		vector.lane[level].step+=dirnode->junction[level].count;
		vector.lane[level].dirnode=next_dirnode;

		if (next_entry->name_next>0) {

		    search_entry=get_entry(next_entry->name_next);
		    search_row+=dirnode->junction[level].count;

		} else {

		    search_entry=next_entry;
		    search_row=dirnode->junction[level].count - 1;

		}

	    }

	}

    }

    /*
	when here, the algoritm has reached the lowest level, and that is the
	linked list of entries
	check first there is not already an exact match found..
	what counts is that entry->name <= name
    */

    if (search_entry!=entry) {

	while (search_entry) {

	    difference=compare_entry(search_entry, name, nameindex_value, lenname);

	    if (difference<0) {

		/* before name still */

		search_entry=get_entry(search_entry->name_next);
		ctr++;
		search_row++;

	    } else if (difference==0) {

		/* exact match */

		entry=search_entry;
		break;

	    } else {

		/* past name: not found */

		break;

	    }

	}

    }

    if (search_entry==entry) {

	*error=0;

	/* remove the different dirnodes */

	remove_dirnodes_sl(directory, &vector, entry);

	/* remove from the linked list */

	if (entry->name_next==(unsigned short) - 1) {

	    /* no next: must be the latest */

	    if (directory->last==entry->index) {

		directory->last=entry->name_prev;

	    } else {

		logoutput("delete_entry_nonempty_sl: internal error, latest entry (%i) differs from directory latest (%i)", entry->index, directory->last);

	    }

	} else {
	    struct notifyfshm_entry_struct *next_entry=NULL;

	    next_entry=get_entry(entry->name_next);
	    next_entry->name_prev=entry->name_prev;

	}

	if (entry->name_prev==(unsigned short) - 1) {

	    /* no prev: must be the first */

	    if (directory->first==entry->index) {

		directory->first=entry->name_next;

	    } else {

		logoutput("delete_entry_nonempty_sl: internal error, first entry (%i) differs from directory first (%i)", entry->index, directory->first);

	    }

	} else {
	    struct notifyfshm_entry_struct *prev_entry=NULL;

	    prev_entry=get_entry(entry->name_prev);
	    prev_entry->name_next=entry->name_next;

	}

	directory->count--;

    }

    out:

    if (row) *row=search_row;

}

void delete_entry_sl_batch(struct notifyfshm_entry_struct *entry, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_entry_struct *parent=get_entry(entry->parent);
    struct notifyfshm_directory_struct *directory=NULL;

    directory=get_directory_sl(parent, 0, error);

    if (directory) {

	delete_entry_nonempty_sl_batch(directory, entry, row, error);

    } else {

	/* no directory found */

	logoutput("delete_entry_sl_batch: no directory");

    }

}
