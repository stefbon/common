/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"

#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"
#include "notifyfs-locking.h"
#include "notifyfs-sl.h"
#include "notifyfs-sl-find.h"

static struct notifyfshm_entry_struct *find_entry_by_name_nonempty_sl(struct notifyfshm_directory_struct *directory, struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_entry_struct *entry=NULL, *search_entry=NULL;
    int level, res=0;
    unsigned int nameindex_value, nameindex_value_first, nameindex_value_last, lenname;
    struct vector_dirnode_struct vector;
    unsigned int search_row=0;

    /* increase the number of users of this skiplist */

    increase_actors(directory);

    if (directory->count==0) goto unlock;

    nameindex_value=calculate_nameindex_value(name, &lenname);

    init_vector_lane(&vector);

    logoutput("find_entry_by_name_nonempty_sl: look for %s, index %i", name, nameindex_value);

    /*
	first check the extreme cases: before the first or the last

	TODO: do something with the information the name to lookup
	is closer to the last than the first

    */

    entry=get_entry(directory->first);

    res=compare_entry(entry, name, nameindex_value, lenname);

    if (res>0) {

	/* before the first: not found */

	*error=ENOENT;
	entry=NULL;
	goto out;

    } else if (res==0) {

	/* exact match: found */

	search_row=1;
	goto out;

    }

    nameindex_value_first=entry->nameindex_value;
    search_entry=entry;
    search_row=1;

    entry=get_entry(directory->last);

    res=compare_entry(entry, name, nameindex_value, lenname);

    if (res<0) {

	/* after the last: not found */

	*error=ENOENT;
	entry=NULL;
	goto out;

    } else if (res==0) {

	/* exact match with last: found */

	search_row=directory->count;
	goto out;

    }

    nameindex_value_last=entry->nameindex_value;
    entry=NULL;

    if ( 2 * nameindex_value < nameindex_value_first + nameindex_value_last) {

	/* closer to the first than the last */

	/*
	    check there are fast lanes
	*/

	if (directory->level>=0) {
	    struct notifyfshm_dirnode_struct *dirnode, *next_dirnode;
	    struct notifyfshm_entry_struct *next_entry=NULL;

	    /* head of the skiplist: there are fast lanes */

	    level=directory->level;

	    vector.maxlevel=level;
	    vector.lane[level].dirnode=get_dirnode(directory->node);
	    vector.direction=1;

	    while(level>=0) {

		/* lock directory */

		lock_directory(directory);

		dirnode=vector.lane[level].dirnode;
		next_dirnode=get_dirnode(dirnode->junction[level].next);

		res=1;

		if (next_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		    next_entry=get_entry(next_dirnode->entry);
		    res=compare_entry(next_entry, name, nameindex_value, lenname);

		}

		if (res>0) {

		    /*
			res>0: this entry is too far, go one level down
			check first it's possible to lock the region (it's not write locked)
		    */

		    if (next_dirnode->lock & NOTIFYFS_DIRNODE_RMLOCK) {

			/* this dirnode is about to be removed: break*/

			*error=EAGAIN;
			vector.minlevel=level+1;
			unlock_directory(directory);
			goto out;

		    } else if (dirnode->junction[level].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

			/* this region is write locked: no readers allowed */

			*error=EAGAIN;
			vector.minlevel=level+1;
			unlock_directory(directory);
			goto out;

		    }

		    vector.lane[level].lockset|=NOTIFYFS_DIRNODE_READLOCK;
		    vector.lockset|=NOTIFYFS_DIRNODE_READLOCK;

		    dirnode->junction[level].lock+=NOTIFYFS_DIRNODE_READLOCK;

		    level--;

		    if (level>=0) vector.lane[level].dirnode=dirnode;

		} else if (res==0) {

		    /* exact match: just ready here ?? */

		    if (next_dirnode->lock&NOTIFYFS_DIRNODE_RMLOCK) {

			*error=ENOENT;

		    } else {

			entry=next_entry;
			search_row+=dirnode->junction[level].count - 1;

		    }

		    vector.minlevel=level+1;
		    unlock_directory(directory);
		    goto out;

		} else {

		    /* res<0: next_entry is smaller than name: skip */

		    vector.lane[level].dirnode=next_dirnode;

		    if (next_entry->name_next==(unsigned short) - 1) {

			search_entry=next_entry;
			search_row+=dirnode->junction[level].count - 1;

		    } else {

			search_entry=get_entry(next_entry->name_next);
			search_row+=dirnode->junction[level].count;

		    }

		}

		unlock_directory(directory);

	    }

	} else {
	    struct notifyfshm_dirnode_struct *dirnode=NULL;

	    lock_directory(directory);

	    dirnode=get_dirnode(directory->node);

	    if (dirnode->junction[0].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

		/* this region is write locked: no readers allowed */

		*error=EAGAIN;
		unlock_directory(directory);
		goto out;

	    }

	    vector.maxlevel=0;
	    vector.direction=1;

	    vector.lane[0].lockset|=NOTIFYFS_DIRNODE_READLOCK;
	    vector.lockset|=NOTIFYFS_DIRNODE_READLOCK;
	    vector.lane[0].dirnode=dirnode;

	    dirnode->junction[0].lock+=NOTIFYFS_DIRNODE_READLOCK;

	    unlock_directory(directory);

	}

	/*
	    walk the linked list, the closest entry is found, go from there
	*/

	if (! entry) {

	    while (search_entry) {

		res=compare_entry(search_entry, name, nameindex_value, lenname);

		if (res<0) {

		    /* before name still */

		    search_entry=get_entry(search_entry->name_next);
		    search_row++;

		} else if (res==0) {

		    /* exact match */
		    entry=search_entry;
		    break;

		} else {

		    /* res>0 : past name, no exact match */
		    break;

		}

	    }

	}

    } else {

	/*
	    closer to the last than the first
	    search from right to left
	*/

	/*
	    check there are fast lanes
	*/

	search_entry=get_entry(directory->last);
	search_row=directory->count;

	if (directory->level>=0) {
	    struct notifyfshm_dirnode_struct *dirnode, *prev_dirnode;
	    struct notifyfshm_entry_struct *prev_entry=NULL;

	    /* head of the skiplist: there are fast lanes */

	    level=directory->level;

	    vector.maxlevel=level;
	    vector.direction=-1;
	    vector.lane[level].dirnode=get_dirnode(directory->node);

	    while(level>=0) {

		/* lock directory */

		lock_directory(directory);

		dirnode=vector.lane[level].dirnode;
		prev_dirnode=get_dirnode(dirnode->junction[level].prev);

		res=-1;

		if (prev_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		    prev_entry=get_entry(prev_dirnode->entry);
		    res=compare_entry(prev_entry, name, nameindex_value, lenname);

		}

		if (res<0) {

		    /*
			res<0: this entry is too small, go one level down
			check first it's possible to lock the region (it's not write locked)
		    */

		    if (prev_dirnode->lock & NOTIFYFS_DIRNODE_RMLOCK) {

			/* this dirnode is about to be removed: break*/

			*error=EAGAIN;
			vector.minlevel=level+1;
			unlock_directory(directory);
			goto out;

		    } else if (prev_dirnode->junction[level].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

			/* this region is write locked: no readers allowed */

			*error=EAGAIN;
			vector.minlevel=level+1;
			unlock_directory(directory);
			goto out;

		    }

		    vector.lane[level].lockset|=NOTIFYFS_DIRNODE_READLOCK;
		    vector.lockset|=NOTIFYFS_DIRNODE_READLOCK;

		    prev_dirnode->junction[level].lock+=NOTIFYFS_DIRNODE_READLOCK;

		    level--;

		    if (level>=0) vector.lane[level].dirnode=dirnode;

		} else if (res==0) {

		    /* exact match: just ready here ?? */

		    if (prev_dirnode->lock&NOTIFYFS_DIRNODE_RMLOCK) {

			*error=ENOENT;

		    } else {

			entry=prev_entry;
			search_row-=(prev_dirnode->junction[level].count - 1);

		    }

		    vector.minlevel=level+1;
		    unlock_directory(directory);
		    goto out;

		} else {

		    /* res>0: prev_entry is bigger than name: skip */

		    vector.lane[level].dirnode=prev_dirnode;

		    /* TODO: adjust when at the tail */

		    if (search_row==directory->count) search_row--;

		    if (prev_entry->name_prev==(unsigned short) - 1) {

			search_entry=prev_entry;
			search_row-=(prev_dirnode->junction[level].count - 1);

		    } else {

			search_entry=get_entry(prev_entry->name_prev);
			search_row-=prev_dirnode->junction[level].count;

		    }

		}

		unlock_directory(directory);

	    }

	} else {
	    struct notifyfshm_dirnode_struct *dirnode=NULL;

	    lock_directory(directory);

	    dirnode=get_dirnode(directory->node);

	    if (dirnode->junction[0].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

		/* this region is write locked: no readers allowed */

		*error=EAGAIN;
		unlock_directory(directory);
		goto out;

	    }

	    vector.maxlevel=0;
	    vector.direction=1;

	    vector.lane[0].lockset|=NOTIFYFS_DIRNODE_READLOCK;
	    vector.lockset|=NOTIFYFS_DIRNODE_READLOCK;
	    vector.lane[0].dirnode=dirnode;

	    dirnode->junction[0].lock+=NOTIFYFS_DIRNODE_READLOCK;

	    unlock_directory(directory);

	}

	/*
	    walk the linked list, the closest entry is found, go from there
	*/

	if (! entry) {

	    while (search_entry) {

		res=compare_entry(search_entry, name, nameindex_value, lenname);

		if (res>0) {

		    /* search entry is too big still, skip */

		    search_entry=get_entry(search_entry->name_prev);
		    search_row--;

		} else if (res==0) {

		    /* exact match */

		    entry=search_entry;
		    break;

		} else {

		    /* search entry is too small : past name, no exact match */

		    break;

		}

	    }

	}

    }

    out:

    if (vector.lockset>0) {

	/* release the locks set on the fast lanes */

	lock_directory(directory);
	unlock_dirnode_vector(&vector, directory);
	cond_broadcast_directory(directory);
	unlock_directory(directory);

    }

    unlock:

    decrease_actors(directory);

    if (row) *row=search_row;

    return entry;

}

struct notifyfshm_entry_struct *find_entry_by_name_sl(struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_directory_struct *directory=NULL;
    struct notifyfshm_entry_struct *entry=NULL;

    // logoutput("find_entry_by_name_sl: find entry with name %s", name);

    if (row) *row=0;

    directory=get_directory_sl(parent, 0, error);

    if (directory) {
	unsigned char counter=0;

	while(counter<10) {

	    entry=find_entry_by_name_nonempty_sl(directory, parent, name, row, error);

	    if (*error==EAGAIN) {

		/* blocking lock: try again, here some timeout? */

		logoutput("find_entry_by_name_sl: operation blocked, try again");
		*error=0;

	    } else {

		break;

	    }

	    counter++;

	}

    } else {

	/* no directory found */

	logoutput("find_entry_by_name_sl: no directory found");
	*error=ENOENT;

    }

    out:

    return entry;

}

static struct notifyfshm_entry_struct *find_entry_by_name_nonempty_sl_batch(struct notifyfshm_directory_struct *directory, struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_entry_struct *entry=NULL, *search_entry=NULL;
    int level, res=0;
    unsigned int nameindex_value, nameindex_value_first, nameindex_value_last, lenname;
    struct vector_dirnode_struct vector;
    unsigned int search_row=0;

    if (directory->count==0) goto out;

    nameindex_value=calculate_nameindex_value(name, &lenname);

    init_vector_lane(&vector);

    // logoutput("find_entry_by_name_nonempty_sl: look for %s", name);

    /*
	first check the extreme cases: before the first or the last

	TODO: do something with the information the name to lookup
	is closer to the last than the first

    */

    entry=get_entry(directory->first);

    res=compare_entry(entry, name, nameindex_value, lenname);

    if (res>0) {

	/* before the first: not found */

	*error=ENOENT;
	entry=NULL;
	goto out;

    } else if (res==0) {

	/* exact match: found */

	search_row=1;
	goto out;

    }

    nameindex_value_first=entry->nameindex_value;
    search_entry=entry;
    search_row=1;

    entry=get_entry(directory->last);

    res=compare_entry(entry, name, nameindex_value, lenname);

    if (res<0) {

	/* after the last: not found */

	*error=ENOENT;
	entry=NULL;
	goto out;

    } else if (res==0) {

	/* exact match with last: found */

	search_row=directory->count;
	goto out;

    }

    nameindex_value_last=entry->nameindex_value;
    entry=NULL;

    if ( 2 * nameindex_value < nameindex_value_first + nameindex_value_last) {

	/* closer to the first than the last */

	/*
	    check there are fast lanes
	*/

	if (directory->level>=0) {
	    struct notifyfshm_dirnode_struct *dirnode, *next_dirnode;
	    struct notifyfshm_entry_struct *next_entry=NULL;

	    /* head of the skiplist: there are fast lanes */

	    level=directory->level;

	    vector.maxlevel=level;
	    vector.lane[level].dirnode=get_dirnode(directory->node);
	    vector.direction=1;

	    while(level>=0) {

		dirnode=vector.lane[level].dirnode;
		next_dirnode=get_dirnode(dirnode->junction[level].next);

		res=1;

		if (next_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		    next_entry=get_entry(next_dirnode->entry);
		    res=compare_entry(next_entry, name, nameindex_value, lenname);

		}

		if (res>0) {

		    /*
			res>0: this entry is too far, go one level down
			check first it's possible to lock the region (it's not write locked)
		    */

		    level--;

		    if (level>=0) vector.lane[level].dirnode=dirnode;

		} else if (res==0) {

		    /* exact match: just ready here ?? */

		    vector.minlevel=level+1;
		    goto out;

		} else {

		    /* res<0: next_entry is smaller than name: skip */

		    vector.lane[level].dirnode=next_dirnode;

		    if (next_entry->name_next==(unsigned short) - 1) {

			search_entry=next_entry;
			search_row+=dirnode->junction[level].count - 1;

		    } else {

			search_entry=get_entry(next_entry->name_next);
			search_row+=dirnode->junction[level].count;

		    }

		}

	    }

	} else {
	    struct notifyfshm_dirnode_struct *dirnode=NULL;

	    dirnode=get_dirnode(directory->node);

	    vector.maxlevel=0;
	    vector.direction=1;

	    vector.lane[0].dirnode=dirnode;

	}

	/*
	    walk the linked list, the closest entry is found, go from there
	*/

	if (! entry) {

	    while (search_entry) {

		res=compare_entry(search_entry, name, nameindex_value, lenname);

		if (res<0) {

		    /* before name still */

		    search_entry=get_entry(search_entry->name_next);
		    search_row++;

		} else if (res==0) {

		    /* exact match */
		    entry=search_entry;
		    break;

		} else {

		    /* res>0 : past name, no exact match */
		    break;

		}

	    }

	}

    } else {

	/*
	    closer to the last than the first
	    search from right to left
	*/

	/*
	    check there are fast lanes
	*/

	search_entry=get_entry(directory->last);
	search_row=directory->count;

	if (directory->level>=0) {
	    struct notifyfshm_dirnode_struct *dirnode, *prev_dirnode;
	    struct notifyfshm_entry_struct *prev_entry=NULL;

	    /* head of the skiplist: there are fast lanes */

	    level=directory->level;

	    vector.maxlevel=level;
	    vector.direction=-1;
	    vector.lane[level].dirnode=get_dirnode(directory->node);

	    while(level>=0) {

		dirnode=vector.lane[level].dirnode;
		prev_dirnode=get_dirnode(dirnode->junction[level].prev);

		res=-1;

		if (prev_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		    prev_entry=get_entry(prev_dirnode->entry);
		    res=compare_entry(prev_entry, name, nameindex_value, lenname);

		}

		if (res<0) {

		    /*
			res<0: this entry is too small, go one level down
			check first it's possible to lock the region (it's not write locked)
		    */

		    level--;
		    if (level>=0) vector.lane[level].dirnode=dirnode;

		} else if (res==0) {

		    /* exact match: just ready here ?? */

		    vector.minlevel=level+1;
		    goto out;

		} else {

		    /* res>0: prev_entry is bigger than name: skip */

		    vector.lane[level].dirnode=prev_dirnode;

		    /* TODO: adjust when at the tail */

		    if (search_row==directory->count) search_row--;

		    if (prev_entry->name_prev==(unsigned short) - 1) {

			search_entry=prev_entry;
			search_row-=(prev_dirnode->junction[level].count - 1);

		    } else {

			search_entry=get_entry(prev_entry->name_prev);
			search_row-=prev_dirnode->junction[level].count;

		    }

		}

	    }

	} else {
	    struct notifyfshm_dirnode_struct *dirnode=NULL;

	    dirnode=get_dirnode(directory->node);

	    vector.maxlevel=0;
	    vector.direction=1;

	    vector.lane[0].dirnode=dirnode;

	}

	/*
	    walk the linked list, the closest entry is found, go from there
	*/

	if (! entry) {

	    while (search_entry) {

		res=compare_entry(search_entry, name, nameindex_value, lenname);

		if (res>0) {

		    /* search entry is too big still, skip */

		    search_entry=get_entry(search_entry->name_prev);
		    search_row--;

		} else if (res==0) {

		    /* exact match */

		    entry=search_entry;
		    break;

		} else {

		    /* search entry is too small : past name, no exact match */

		    break;

		}

	    }

	}

    }

    out:

    if (row) *row=search_row;

    return entry;

}

struct notifyfshm_entry_struct *find_entry_by_name_sl_batch(struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error)
{
    struct notifyfshm_directory_struct *directory=NULL;
    struct notifyfshm_entry_struct *entry=NULL;

    // logoutput("find_entry_by_name_sl_batch: find entry with name %s", name);

    if (row) *row=0;

    directory=get_directory_sl(parent, 0, error);

    if (directory) {

	entry=find_entry_by_name_nonempty_sl_batch(directory, parent, name, row, error);

    } else {

	/* no directory found */

	logoutput("find_entry_by_name_sl: no directory found");
	*error=ENOENT;

    }

    out:

    return entry;

}

struct notifyfshm_entry_struct *find_entry_by_row_sl(struct notifyfshm_entry_struct *parent, unsigned int row)
{
    struct notifyfshm_entry_struct *entry=NULL;
    struct notifyfshm_directory_struct *directory=NULL;
    unsigned int error=0;

    directory=get_directory_sl(parent, 0, &error);

    if (directory) {
	int level;
	struct notifyfshm_dirnode_struct *dirnode;
	struct notifyfshm_entry_struct *search_entry=NULL;

	increase_actors(directory);

	level=directory->level;

	dirnode=get_dirnode(directory->node);
	search_entry=get_entry(directory->first);

	while (level>=0) {

	    if (row>dirnode->junction[level].count) {

		/* hop to the next on the same fast lane */

		row-=dirnode->junction[level].count;

		dirnode=get_dirnode(dirnode->junction[level].next);

		if (dirnode->entry<0) {

		    /* back at head: end of fast lane */

		    dirnode=NULL;
		    break;

		}

		search_entry=get_entry(dirnode->entry);

	    } else if (row==dirnode->junction[level].count) {

		/* exact row found */

		entry=get_entry(dirnode->entry);
		dirnode=NULL;
		break;

	    } else {

		/* next hop is too far */

		level--;

	    }

	}

	if (!entry) {

	    entry=search_entry;

	    while(entry && row>0) {

		entry=get_entry(entry->name_next);
		row--;

	    }

	}

	decrease_actors(directory);

    }


    out:

    return entry;

}

