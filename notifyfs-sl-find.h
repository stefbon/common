/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef _NOTIFYFS_SL_FIND_H
#define _NOTIFYFS_SL_FIND_H


/* find an entry in a skip list (=directory)*/

struct notifyfshm_entry_struct *find_entry_by_name_sl(struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error);

struct notifyfshm_entry_struct *find_entry_by_name_sl_batch(struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error);

struct notifyfshm_entry_struct *find_entry_by_row_sl(struct notifyfshm_entry_struct *parent, unsigned int row);


#endif
