/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"

#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"
#include "notifyfs-locking.h"
#include "notifyfs-sl.h"
#include "notifyfs-sl-insert.h"

/* TODO: make this in notifyfs, not in common */

static int do_levelup(struct notifyfshm_directory_struct *directory)
{
    int level=-1;
    unsigned long hlpctr=directory->slctr;

    lock_directory(directory);

    hlpctr++;

    /* determine the level by testing how many times the hlpctr is dividable by PROB */

    while(hlpctr % NOTIFYFS_SKIPLIST_PROB == 0) {

	level++;
	if (level>directory->level || level==NOTIFYFS_SKIPLIST_MAXLEVEL-1) break;

	hlpctr/=NOTIFYFS_SKIPLIST_PROB;

    }

    unlock_directory(directory);

    return level;

}

static unsigned int update_counters(int ctr_level, struct notifyfshm_directory_struct *directory, struct vector_dirnode_struct *vector, int ctr_left)
{
    struct notifyfshm_dirnode_struct *dirnode=NULL;

    /*
	the lanes where only the counters are updated
	this loop is only done when the level upto where dirnodes are inserted (newlevel)
	is smaller than the current directory->level
	[max_insertlevel+1, directory->level]
    */

    while(ctr_level<=directory->level) {

	dirnode=vector->lane[ctr_level].dirnode;

	dirnode->junction[ctr_level].count++;
	ctr_left+=vector->lane[ctr_level].step;

	/* correct the lock if this has been set */

	if (dirnode->junction[ctr_level].lock & vector->lane[ctr_level].lockset) {

	    dirnode->junction[ctr_level].lock-=vector->lane[ctr_level].lockset;

	}

	vector->lane[ctr_level].lockset=0;
	vector->lane[ctr_level].dirnode=NULL;

	ctr_level++;

    }

    return ctr_left;

}

static unsigned int add_dirnodes_sl(struct notifyfshm_directory_struct *directory, struct vector_dirnode_struct *vector, int ctr_left, struct notifyfshm_entry_struct *entry, int newlevel)
{
    int ctr_level=0;
    struct notifyfshm_dirnode_struct *dirnode=NULL;

    if (newlevel>=0) {
	int max_insertlevel=(newlevel<directory->level) ? newlevel : directory->level;
	struct notifyfshm_dirnode_struct *new_dirnode=NULL;

	new_dirnode=notifyfshm_malloc_dirnode();

	if (new_dirnode) {
	    struct notifyfshm_dirnode_struct *next_dirnode=NULL;

	    /*
		the existing lanes where a dirnode is inserted
		[0, max_insertlevel]
	    */

	    init_dirnode(new_dirnode);

	    new_dirnode->entry=entry->index;
	    new_dirnode->type=NOTIFYFS_DIRNODE_TYPE_BETWEEN;

	    while(ctr_level<=max_insertlevel) {

		dirnode=vector->lane[ctr_level].dirnode;

		next_dirnode=get_dirnode(dirnode->junction[ctr_level].next);

		/* insert between dirnode and next */

		new_dirnode->junction[ctr_level].prev=dirnode->index;
		new_dirnode->junction[ctr_level].next=next_dirnode->index;

		dirnode->junction[ctr_level].next=new_dirnode->index;
		next_dirnode->junction[ctr_level].prev=new_dirnode->index;

		/* change the counters */

		if (dirnode->junction[ctr_level].count>0) {

		    new_dirnode->junction[ctr_level].count=dirnode->junction[ctr_level].count - ctr_left + 1;

		} else {

		    /* count is zero: first time here */

		    new_dirnode->junction[ctr_level].count=directory->count - ctr_left + 1;

		}

		dirnode->junction[ctr_level].count=ctr_left;

		/* update the counter left with the steps taken */

		ctr_left+=vector->lane[ctr_level].step;

		/* correct the lock if this has been set */

		if (dirnode->junction[ctr_level].lock & vector->lane[ctr_level].lockset) dirnode->junction[ctr_level].lock-=vector->lane[ctr_level].lockset;

		vector->lane[ctr_level].lockset=0;
		vector->lane[ctr_level].dirnode=NULL;

		ctr_level++;

	    }

	    /*
		the lanes to be created
		this loop is only done when newlevel > directory->level
		(20130826: newlevel is never bigger than directory->level + 1)
		[directory->level+1, newlevel]

	    */

	    while(ctr_level<=newlevel) {

		dirnode=get_dirnode(directory->node);

		/* insert in the new fast lane */

		new_dirnode->junction[ctr_level].next=dirnode->index;
		new_dirnode->junction[ctr_level].prev=dirnode->index;

		dirnode->junction[ctr_level].next=new_dirnode->index;
		dirnode->junction[ctr_level].prev=new_dirnode->index;

		/* correct the level of the directory */

		directory->level=ctr_level;

		/* assign counters */

		if (dirnode->junction[ctr_level].count>0) {

		    new_dirnode->junction[ctr_level].count=dirnode->junction[ctr_level].count - ctr_left + 1;

		} else {

		    /* count is zero: first time here */

		    new_dirnode->junction[ctr_level].count=directory->count - ctr_left + 1;

		}

		dirnode->junction[ctr_level].count=ctr_left;

		vector->lane[ctr_level].dirnode=NULL;

		ctr_level++;

	    }

	} else {

	    logoutput("add_dirnodes_sl: error allocating dirnodes");

	}

    }

    /* update the remaining counters */

    ctr_left=update_counters(ctr_level, directory, vector, ctr_left);

    cond_broadcast_directory(directory);

    return ctr_left;

}

static struct notifyfshm_entry_struct *insert_entry_nonempty_sl(struct notifyfshm_entry_struct *parent, struct notifyfshm_directory_struct *directory, const char *name, unsigned int *row, int newlevel, unsigned int *error, struct notifyfshm_entry_struct *(*create_entry_cb) (struct notifyfshm_entry_struct *parent, const char *name), struct notifyfshm_entry_struct *entry)
{
    struct notifyfshm_entry_struct *search_entry=NULL, *entry_found=NULL;
    int level, difference;
    struct vector_dirnode_struct vector;
    unsigned int nameindex_value, lenname;
    unsigned char exclusive=0;
    unsigned int search_row=0;
    unsigned int ctr=0;

    increase_actors(directory);

    *error=ENOENT;

    if (directory->count==0) {

	/* when empty just add it and leave */

	get_directory_exclusive(directory);
	exclusive=1;

	if (entry) {
	    unsigned int lenname=0;

	    directory->first=entry->index;
	    directory->last=entry->index;
	    directory->count=1;

	    if (entry->nameindex_value==0) entry->nameindex_value=calculate_nameindex_value(name, &lenname);

	    if (row) *row=1;
	    entry_found=entry;
	    *error=0;

	} else if (name && create_entry_cb) {

	    entry=create_entry_cb(parent, name);

	    if (entry) {
		unsigned int lenname=0;

		directory->first=entry->index;
		directory->last=entry->index;
		directory->count=1;

		entry->nameindex_value=calculate_nameindex_value(name, &lenname);

		if (row) *row=1;
		entry_found=entry;
		*error=0;

	    } else {

		*error=ENOMEM;

	    }

	}

	goto unlockexclusive;

    } else if (newlevel>directory->level) {

	if (prepare_directory_exclusive(directory, 0)==-1) {

	    logoutput("insert_entry_nonempty_sl: (%s), newlevel %i prepare lock exclusive failed", name, newlevel);

	    *error=EAGAIN;
	    goto out;

	} else {

	    logoutput("insert_entry_nonempty_sl: (%s), newlevel %i prepare lock exclusive success", name, newlevel);

	}

	exclusive=1;

    }

    nameindex_value=calculate_nameindex_value(name, &lenname);

    init_vector_lane(&vector);

    search_entry=get_entry(directory->first);
    search_row=1;
    ctr=1;

    if (directory->level>=0) {
	struct notifyfshm_dirnode_struct *dirnode, *next_dirnode;
	struct notifyfshm_entry_struct *next_entry=NULL;

	/* head of the skiplist */

	level=directory->level;

	vector.maxlevel=level;
	vector.direction=1;
	vector.lane[level].dirnode=get_dirnode(directory->node);

	while(level>=0) {

	    lock_directory(directory);

	    dirnode=vector.lane[level].dirnode;
	    next_dirnode=get_dirnode(dirnode->junction[level].next);

	    difference=1;

	    if (next_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		next_entry=get_entry(next_dirnode->entry);

		difference=compare_entry(next_entry, name, nameindex_value, lenname);

	    }

	    if (difference>0) {

		/*
		    next dirnode is too far
		    here try to lock the region in this lane
		*/

		if (next_dirnode->lock & NOTIFYFS_DIRNODE_RMLOCK) {

		    /* this dirnode is about to be removed: break */

		    *error=EAGAIN;
		    vector.minlevel=level+1;
		    unlock_directory(directory);
		    goto out;

		} else if (dirnode->junction[level].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

		    /* this region in this fast lane is already locked */

		    *error=EAGAIN;
		    vector.minlevel=level+1;
		    unlock_directory(directory);
		    goto out;

		} else {

		    dirnode->junction[level].lock|=NOTIFYFS_DIRNODE_WRITELOCK;
		    vector.lane[level].lockset=NOTIFYFS_DIRNODE_WRITELOCK;
		    vector.lockset|=NOTIFYFS_DIRNODE_WRITELOCK;

		    if (dirnode->junction[level].lock>>3 > 0) {

			/* there are readers active here: wait for them to finish */

			while(dirnode->junction[level].lock>>3 > 0) {

			    cond_wait_directory(directory);

			}

		    }

		}

		level--;
		if (level>=0) vector.lane[level].dirnode=dirnode;

	    } else if (difference==0) {

		if (next_dirnode->lock&NOTIFYFS_DIRNODE_RMLOCK) {

		    /* the entry to insert is about to be removed: try again later */

		    *error=EAGAIN;

		} else {

		    *error=EEXIST;
		    search_row+=dirnode->junction[level].count - 1;
		    entry_found=next_entry;

		}

		vector.minlevel=level+1;
		unlock_directory(directory);
		goto out;

	    } else {

		/* difference<0: skip, stay on the same fast lane */

		vector.lane[level].step+=dirnode->junction[level].count;
		vector.lane[level].dirnode=next_dirnode;

		if (next_entry->name_next==(unsigned short) - 1) {

		    search_entry=next_entry;
		    search_row+=dirnode->junction[level].count - 1;
		    ctr=0;

		} else {

		    search_entry=get_entry(next_entry->name_next);
		    search_row+=dirnode->junction[level].count;
		    ctr=1;

		}

	    }

	    unlock_directory(directory);

	}

    } else {
	struct notifyfshm_dirnode_struct *dirnode;

	/* no fast lanes: here lock level 0 */

	lock_directory(directory);

	dirnode=get_dirnode(directory->node);
	vector.lane[0].dirnode=dirnode;
	vector.maxlevel=0;
	vector.direction=1;

	if (dirnode->junction[0].lock & NOTIFYFS_DIRNODE_WRITELOCK) {

	    /* this region is already locked */

	    *error=EAGAIN;

	    unlock_directory(directory);
	    goto out;

	} else {

	    dirnode->junction[0].lock|=NOTIFYFS_DIRNODE_WRITELOCK;
	    vector.lane[0].lockset=NOTIFYFS_DIRNODE_WRITELOCK;
	    vector.lockset|=NOTIFYFS_DIRNODE_WRITELOCK;

	    if (dirnode->junction[0].lock>>3 > 0) {

		/* there are readers active here: wait for them to finish */

		while(dirnode->junction[0].lock>>3 > 0) {

		    cond_wait_directory(directory);

		}

	    }

	}

	unlock_directory(directory);

    }

    /*
	when here, the algoritm has reached the lowest level, and that is the
	linked list of entries
	check first there is not already an exact match found..
	what counts is that entry->name <= name
    */

    if (*error==ENOENT) {

	while (search_entry) {

	    difference=compare_entry(search_entry, name, nameindex_value, lenname);

	    if (difference<0) {

		/* before name still */

		search_entry=get_entry(search_entry->name_next);
		search_row++;
		ctr++;
		continue;

	    } else if (difference==0) {

		/* exact match */

		*error=EEXIST;
		entry_found=search_entry;
		break;

	    } else {

		/* next entry is bigger: insert here */

		break;

	    }

	}

	if (*error==ENOENT) {

	    if (exclusive==1) {

		get_directory_exclusive(directory);

	    } else {

		lock_directory(directory);

	    }

	    if (! entry) entry=create_entry_cb(parent, name);

	    if (entry) {

		entry->nameindex_value=nameindex_value;
		entry->name_prev=(unsigned short) - 1;
		entry->name_next=(unsigned short) - 1;

		entry_found=entry;

		*error=0; /* success */

		if (search_entry) {

		    /* there is a next entry */

		    entry->name_prev=search_entry->name_prev;
		    entry->name_next=search_entry->index;

		    /* only create a dirnode when in between */

		    if (search_entry->name_prev==(unsigned short) - 1) {

			ctr=update_counters(0, directory, &vector, ctr);
			directory->first=entry->index;

		    } else {

			struct notifyfshm_entry_struct *prev_entry=get_entry(search_entry->name_prev);
			struct notifyfshm_dirnode_struct *dirnode=vector.lane[0].dirnode;

			prev_entry->name_next=entry->index;

			if (ctr>1 && ctr<dirnode->junction[0].count-1) {

			    /* only add dirnodes when not too close to another */

			    ctr=add_dirnodes_sl(directory, &vector, ctr, entry, newlevel);
			    directory->slctr++;

			} else {

			    ctr=update_counters(0, directory, &vector, ctr);

			}

		    }

		    search_entry->name_prev=entry->index;

		} else {

		    /*
			special case:

			a. no search entry can mean the search ended at the last entry
			b. or no entry at all: directory is empty

			in both 
		    */

		    if (directory->count==0) {

			directory->first=entry->index;
			directory->last=entry->index;

			search_row=1;

		    } else {
			struct notifyfshm_entry_struct *prev_entry=get_entry(directory->last);

			ctr=update_counters(0, directory, &vector, ctr);

			prev_entry->name_next=entry->index;
			entry->name_prev=prev_entry->index;

			directory->last=entry->index;

			search_row=directory->count+1;

		    }

		}

		directory->count++;

	    } else {

		*error=ENOMEM;

	    }

	    if (exclusive==1) {

		unlock_directory_exclusive(directory);
		exclusive=0;

	    } else {

		unlock_directory(directory);

	    }

	}

    }

    out:

    if (row) *row=search_row;

    if (vector.lockset>0) {

	/*
	    release the locks set on the fast lanes
	    it's possible that these locks has been removed above when inserting the dirnode
	*/

	lock_directory(directory);
	unlock_dirnode_vector(&vector, directory);
	cond_broadcast_directory(directory);
	unlock_directory(directory);

    }

    unlockexclusive:

    if (exclusive==1) unlock_directory_exclusive(directory);

    decrease_actors(directory);

    if (entry_found) {

	logoutput("insert_entry_nonempty_sl: entry (%s) inserted at row %i", name, search_row);

    } else {

	logoutput("insert_entry_nonempty_sl: (%s) error %i", name, *error);

    }

    return entry_found;

}

struct notifyfshm_entry_struct *insert_entry_sl(struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error, struct notifyfshm_entry_struct *(*create_entry_cb) (struct notifyfshm_entry_struct *parent, const char *name), struct notifyfshm_entry_struct *entry)
{
    struct notifyfshm_directory_struct *directory=NULL;

    if (row) *row=0;

    directory=get_directory_sl(parent, 1, error);

    if (directory) {

	/*
	    different cases
	    - insert before the first (no extra dirnodes)
	    - insert after last (no extra dirnodes)
	    - name is closer to begin
	    - name is closer to end

	*/

	int newlevel;
	unsigned int safe_counter=0;

	/*
	    determine the level of the fast lane in which this entry will be present 
	    (very important to set it once outside the while loop)
	*/

	newlevel=do_levelup(directory);

	while(safe_counter<20) {

	    entry=insert_entry_nonempty_sl(parent, directory, name, row, newlevel, error, create_entry_cb, entry);

	    if (*error==0) {

		break;

	    } else if (*error==EAGAIN) {

		logoutput("insert_entry_sl: (%s) operation blocked, try again", name);

	    } else {

		logoutput("insert_entry_sl: (%s) error %i", name, *error);
		break;

	    }

	    safe_counter++;

	}

    } else {

	/* no directory found */

	logoutput("insert_entry_sl: (%s) no directory found", name);

    }


    return entry;

}

static struct notifyfshm_entry_struct *insert_entry_nonempty_sl_batch(struct notifyfshm_entry_struct *parent, struct notifyfshm_directory_struct *directory, const char *name, struct notifyfshm_entry_struct *(*create_entry_cb) (struct notifyfshm_entry_struct *parent, const char *name), unsigned int *row, int newlevel, unsigned int *error)
{
    struct notifyfshm_entry_struct *search_entry=NULL, *entry=NULL;
    int level, difference;
    struct vector_dirnode_struct vector;
    unsigned int nameindex_value, lenname;
    unsigned int search_row=0;
    unsigned int ctr=0;

    nameindex_value=calculate_nameindex_value(name, &lenname);

    init_vector_lane(&vector);

    *error=ENOENT;

    search_entry=get_entry(directory->first);
    search_row=1;
    ctr=1;

    if (directory->level>=0) {
	struct notifyfshm_dirnode_struct *dirnode, *next_dirnode;
	struct notifyfshm_entry_struct *next_entry=NULL;

	/* head of the skiplist */

	level=directory->level;

	vector.maxlevel=level;
	vector.lane[level].dirnode=get_dirnode(directory->node);
	vector.direction=1;

	while(level>=0) {

	    dirnode=vector.lane[level].dirnode;
	    next_dirnode=get_dirnode(dirnode->junction[level].next);

	    difference=1;

	    if (next_dirnode->type & NOTIFYFS_DIRNODE_TYPE_BETWEEN) {

		next_entry=get_entry(next_dirnode->entry);
		difference=compare_entry(next_entry, name, nameindex_value, lenname);

	    }

	    if (difference>0) {

		/* next entry on next dirnode is too far: go one level down */

		level--;
		if (level>=0) vector.lane[level].dirnode=dirnode;

	    } else if (difference==0) {

		vector.minlevel=level+1;
		entry=next_entry;
		*error=EEXIST;
		goto out;

	    } else {

		/* still bigger than next entry on next dirnode: skip */

		vector.lane[level].step+=dirnode->junction[level].count;
		vector.lane[level].dirnode=next_dirnode;

		if (next_entry->name_next==(unsigned short) - 1) {

		    search_entry=next_entry;
		    search_row+=dirnode->junction[level].count - 1;
		    ctr=0;

		} else {

		    search_entry=get_entry(next_entry->name_next);
		    search_row+=dirnode->junction[level].count;
		    ctr=1;

		}

	    }

	}

    } else {
	struct notifyfshm_dirnode_struct *dirnode;

	dirnode=get_dirnode(directory->node);
	vector.lane[0].dirnode=dirnode;
	vector.maxlevel=0;
	vector.direction=1;

    }

    /*
	when here, the algoritm has reached the lowest level, and that is the
	linked list of entries
	check first there is not already an exact match found..
	what counts is that entry->name <= name
    */

    if (*error==ENOENT) {

	while (search_entry) {

	    difference=compare_entry(search_entry, name, nameindex_value, lenname);

	    if (difference<0) {

		/* before name still */

		search_entry=get_entry(search_entry->name_next);
		search_row++;
		ctr++;
		continue;

	    } else if (difference==0) {

		/* exact match */

		*error=EEXIST;
		entry=search_entry;
		break;

	    } else {

		/* next entry is bigger: insert here */

		break;

	    }

	}

	if (*error==ENOENT) {

	    entry=create_entry_cb(parent, name);

	    if (entry) {

		entry->nameindex_value=nameindex_value;
		entry->name_prev=(unsigned short) - 1;
		entry->name_next=(unsigned short) - 1;

		*error=0; /* success */

		if (search_entry) {
		    struct notifyfshm_entry_struct *prev_entry=get_entry(search_entry->name_prev);

		    /* there is a next entry */

		    entry->name_prev=search_entry->name_prev;
		    entry->name_next=search_entry->index;

		    /* only create a dirnode when in between */

		    if (! prev_entry) {

			ctr=update_counters(0, directory, &vector, ctr);
			directory->first=entry->index;

		    } else {

			prev_entry->name_next=entry->index;

			ctr=add_dirnodes_sl(directory, &vector, ctr, entry, newlevel);

		    }

		    search_entry->name_prev=entry->index;

		} else {
		    struct notifyfshm_entry_struct *prev_entry=get_entry(directory->last);

		    /*
			special case:

			a. no search entry can mean the search ended at the last entry
			b. or no entry at all: directory is empty

			in both 
		    */

		    if (! prev_entry) {

			directory->first=entry->index;
			directory->last=entry->index;

			search_row=1;

		    } else {

			update_counters(0, directory, &vector, ctr);

			prev_entry->name_next=entry->index;
			entry->name_prev=prev_entry->index;

			directory->last=entry->index;

			search_row=directory->count+1;

		    }

		}

		directory->count++;
		directory->slctr++;


	    } else {

		*error=ENOMEM;

	    }

	}

    }

    out:

    if (row) *row=search_row;

    return entry;

}

struct notifyfshm_entry_struct *insert_entry_sl_batch(struct notifyfshm_directory_struct *directory, struct notifyfshm_entry_struct *parent, const char *name, unsigned int *row, unsigned int *error, struct notifyfshm_entry_struct *(*create_entry_cb) (struct notifyfshm_entry_struct *parent, const char *name))
{
    struct notifyfshm_entry_struct *entry=NULL;
    int newlevel;

    if (row) *row=0;

    if (directory) {

	logoutput("insert_entry_sl_batch: directory set");

    } else {

	logoutput("insert_entry_sl_batch: directory not set");

    }

    newlevel=do_levelup(directory);
    *error=0;

    entry=insert_entry_nonempty_sl_batch(parent, directory, name, create_entry_cb, row, newlevel, error);

    return entry;

}

