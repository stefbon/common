/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>


#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"

#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"
#include "notifyfs-locking.h"
#include "notifyfs-sl.h"
#include "notifyfs-sl-find.h"
#include "notifyfs-sl-utils.h"

/* function which does a lookup of the next entry with the same parent 
    used for getting the contents of a directory */

struct notifyfshm_entry_struct *get_next_entry(struct notifyfshm_entry_struct *parent, struct notifyfshm_entry_struct *entry)
{

    if (entry) {

	/* here TODO: locking */

	if (entry->name_next==(unsigned short) - 1) {

	    entry=NULL;

	} else {

	    entry=get_entry(entry->name_next);

	}

    } else {
	unsigned int error=0;
	struct notifyfshm_directory_struct *directory=get_directory_sl(parent, 0, &error);

	if (directory) {

	    entry=get_entry(directory->first);

	}

    }

    return entry;

}


struct notifyfshm_entry_struct *check_notifyfs_path(char *path)
{
    char *pos, *slash;
    struct notifyfshm_entry_struct *parent_entry, *entry=NULL;
    unsigned int error=0;

    logoutput("check_notifyfs_path: path %s", path);

    parent_entry=get_entry(0);
    entry=parent_entry;

    pos=path;

    while(1) {

        /*  walk through path from begin to end and 
            check every part */

        slash=strchr(pos, '/');

	if ( ! slash) {

	    if (strlen(pos)==0) break;

	} else if ( slash==pos ) {

            /* ignore the starting slash*/

            pos++;

            /* if nothing more (==only a slash) stop here */

            if (strlen(pos)==0) {

        	// entry=parent_entry;
        	break;

	    }

            continue;

        }

        if ( slash ) {

	    /* replace the slash by a \0: make the name a string: zero terminated */

            *slash='\0';

    	    entry=find_entry_by_name_sl(parent_entry, pos, NULL, &error);

            /* make slash a slash again (was turned into a \0) */

	    if (entry) {

		logoutput("check_notifyfs_path: name %s found", pos);

	    } else {

		logoutput("check_notifyfs_path: name %s not found", pos);

	    }

            *slash='/';
            pos=slash+1;

	    if (! entry) break;

        } else {

	    entry=find_entry_by_name_sl(parent_entry, pos, NULL, &error);

	    if (entry) {

		logoutput("check_notifyfs_path: name %s found", pos);

	    } else {

		logoutput("check_notifyfs_path: name %s not found", pos);

	    }

	    break;

	}

	parent_entry=entry;

    }

    out:

    // logoutput("check_notifyfs_path: ready");

    return entry;

}

