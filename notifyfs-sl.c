/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#define LOG_LOGAREA LOG_LOGAREA_INODES

#include "logging.h"

#include "notifyfs-fsevent.h"
#include "notifyfs-shm.h"
#include "notifyfs-locking.h"
#include "notifyfs-sl.h"

static struct vector_dirnode_struct zero_vector;
static pthread_mutex_t createdirectory_mutex=PTHREAD_MUTEX_INITIALIZER;

void init_notifyfshm_sl()
{
    unsigned int i;

    zero_vector.maxlevel=0;
    zero_vector.minlevel=0;
    zero_vector.lockset=0;
    zero_vector.direction=0;

    for (i=0;i<NOTIFYFS_SKIPLIST_MAXLEVEL;i++) {

	zero_vector.lane[i].dirnode=NULL;
	zero_vector.lane[i].step=0;
	zero_vector.lane[i].lockset=0;

    }

}

void init_vector_lane(struct vector_dirnode_struct *vector)
{

    memcpy(vector, &zero_vector, sizeof(struct vector_dirnode_struct));

}

unsigned int calculate_nameindex_value(const char *name, unsigned int *lenname)
{
    unsigned int nameindex_value=0;
    unsigned char firstletter;

    firstletter=*(name)-32;

    *lenname=strlen(name);

    if ((*lenname)>=4) {
	unsigned char secondletter=0;
	unsigned char thirdletter=0;
	unsigned char fourthletter=0;

	secondletter=*(name+1)-32;
	thirdletter=*(name+2)-32;
	fourthletter=*(name+3)-32;

	nameindex_value+=(firstletter * NAMEINDEX_ROOT3) + (secondletter * NAMEINDEX_ROOT2) + (thirdletter * NAMEINDEX_ROOT1) + fourthletter;

    } else if ((*lenname)==3) {
	unsigned char secondletter=0;
	unsigned char thirdletter=0;

	secondletter=*(name+1)-32;
	thirdletter=*(name+2)-32;

	nameindex_value+=(firstletter * NAMEINDEX_ROOT3) + (secondletter * NAMEINDEX_ROOT2) + (thirdletter * NAMEINDEX_ROOT1);

    } else if ((*lenname)==2) {
	unsigned char secondletter=0;

	secondletter=*(name+1)-32;

	nameindex_value+=(firstletter * NAMEINDEX_ROOT3) + (secondletter * NAMEINDEX_ROOT2);

    } else {

	/* len is one */

        nameindex_value+=(firstletter * NAMEINDEX_ROOT3);

    }

    return nameindex_value;

}

int compare_entry(struct notifyfshm_entry_struct *entry, const char *name, unsigned int nameindex_value, int len0)
{
    int res=0;

    if (entry->nameindex_value>nameindex_value) {

	res=1;

    } else if ( nameindex_value==entry->nameindex_value) {
	char *entry_name;

	entry_name=get_data(entry->name);

	if (len0>4) {
	    int len1=strlen(entry_name);

	    if ( len1>4) {

		/*
		    the same nameindex value, test futher only if name is longer than the base
		    when not that long, the names are equal, so use the default value 0

		    and it's only required to compare the part after the first NAMEINDEX_BASE characters
		    since the first 4 characters are the same

		    note that a check on the length of entry_name is not required, since the 

		*/

		res=strcmp(entry_name+4, name+4);

	    } else {

		/*
		    only possible len1==4
		    otherwise the nameindex values would not be the same

		    but here: name>entry_name
		*/

		res=-1;

	    }

	} else if (len0==4) {
	    int len1=strlen(entry_name);

	    /*
		the only possibilities are that len1==len0 and len1>len0
		it's not possible that len1<len0, since they have the same nameindex value
	    */

	    if (len1>4) {

		res=1;

	    } else {

		res=0;

	    }

	} else {

	    /* len0<4, and since the nameindex values are the same, len1 is equal to len0 and the names are equal */

	    res=0;

	}

    } else {

	res=-1;

    }

    return res;

}

void unlock_dirnode_vector(struct vector_dirnode_struct *vector, struct notifyfshm_directory_struct *directory)
{
    unsigned char ctr=vector->minlevel;
    struct notifyfshm_dirnode_struct *dirnode=NULL;

    if (vector->direction==1) {

	while(ctr<=vector->maxlevel) {

	    dirnode=vector->lane[ctr].dirnode;

	    if (dirnode) {
		// struct notifyfshm_dirnode_struct *next_dirnode=get_dirnode(dirnode->junction[ctr].next);;
		// struct notifyfshm_entry_struct *begin_entry=(dirnode->entry>=0) ? get_entry(dirnode->entry) : get_entry(directory->first);
		// struct notifyfshm_entry_struct *end_entry=(next_dirnode->entry>=0) ? get_entry(next_dirnode->entry) : get_entry(directory->last);
		// char *b_name=get_data(begin_entry->name);
		// char *e_name=get_data(end_entry->name);

		// logoutput("unlock_dirnode_vector: found lock region %s - %s level %i locked %i, set %i", b_name, e_name, ctr, dirnode->junction[ctr].lock, vector->lane[ctr].lockset);

		dirnode->junction[ctr].lock-=vector->lane[ctr].lockset;

	    }

	    vector->lane[ctr].lockset=0;
	    ctr++;

	}

    } else if (vector->direction==-1) {

	while(ctr<=vector->maxlevel) {

	    dirnode=vector->lane[ctr].dirnode;

	    /* when going from right to left the lock has been set on the previous */

	    dirnode=(dirnode) ? get_dirnode(dirnode->junction[ctr].prev) : NULL;

	    if (dirnode) {

		dirnode->junction[ctr].lock-=vector->lane[ctr].lockset;

	    }

	    vector->lane[ctr].lockset=0;
	    ctr++;

	}

    }

}

static struct notifyfshm_directory_struct *create_directory_sl(struct notifyfshm_entry_struct *entry, unsigned int *error)
{
    struct notifyfshm_directory_struct *directory=NULL;
    struct notifyfshm_dirnode_struct *dirnode=NULL;

    directory=notifyfshm_malloc_directory();

    if (directory) {

	directory->level=0;
	directory->node=(unsigned short) - 1;
	directory->first=(unsigned short) - 1;
	directory->last=(unsigned short) - 1;
	directory->count=0;
	directory->synctime.tv_sec=0;
	directory->synctime.tv_nsec=0;

	directory->shared_lock=(unsigned short) - 1;

	dirnode=notifyfshm_malloc_dirnode();

	if (dirnode) {
	    unsigned char i;

	    directory->node=dirnode->index;

	    dirnode->entry=(unsigned short) - 1;
	    dirnode->lock=0;
	    dirnode->type=NOTIFYFS_DIRNODE_TYPE_START;

	    for (i=0;i<NOTIFYFS_SKIPLIST_MAXLEVEL;i++) {

		dirnode->junction[i].lock=0;
		dirnode->junction[i].next=dirnode->index;
		dirnode->junction[i].prev=dirnode->index;
		dirnode->junction[i].count=0;

	    }

	    *error=0;

	} else {

	    *error=ENOMEM;

	    goto error;

	}

	entry->type.directory=directory->index;

	return directory;

    } else {

	*error=ENOMEM;

    }

    error:

    if (dirnode) free_dirnode(dirnode);
    free_directory(directory);

    return NULL;

}

struct notifyfshm_directory_struct *get_directory_sl(struct notifyfshm_entry_struct *parent, unsigned char create, unsigned int *error)
{
    struct notifyfshm_directory_struct *directory=NULL;

    if (S_ISDIR(parent->c_mode)) {

	pthread_mutex_lock(&createdirectory_mutex);

	directory=get_directory(parent->type.directory);
	*error=0;

	if (! directory && create==1) {
	    char *name=get_data(parent->name);

	    logoutput("get_directory_sl: create directory for %s", name);

	    directory=create_directory_sl(parent, error);

	}

	pthread_mutex_unlock(&createdirectory_mutex);

    } else {

	*error=ENOTDIR;

    }

    return directory;

}
