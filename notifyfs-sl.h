/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef _NOTIFYFS_SL_H
#define _NOTIFYFS_SL_H

#define NOTIFYFS_SKIPLIST_PROB					4

/* lock set on an entire dirnode when exact match is found by delete */
#define NOTIFYFS_DIRNODE_RMLOCK			1

/* when readers are active in a lane/region and write lock is required, set this lock to prevent futher readers */
#define NOTIFYFS_DIRNODE_BLOCKLOCK		2

/* lock set in a lane/region to inserte/remove an entry */
#define NOTIFYFS_DIRNODE_WRITELOCK		4

/* lock set in a lane/region to read */
#define NOTIFYFS_DIRNODE_READLOCK		8


struct step_dirnode_struct {
    struct notifyfshm_dirnode_struct *dirnode;
    unsigned char lockset;
    unsigned int step;
};

struct vector_dirnode_struct {
    struct step_dirnode_struct lane[NOTIFYFS_SKIPLIST_MAXLEVEL];
    unsigned char maxlevel;
    unsigned char minlevel;
    unsigned char lockset;
    signed char direction;
};

/* prototypes */

void init_notifyfshm_sl();
void init_vector_lane(struct vector_dirnode_struct *vector);

void unlock_dirnode_vector(struct vector_dirnode_struct *vector, struct notifyfshm_directory_struct *directory);

/* calculate the name index value used in the internal index */

unsigned int calculate_nameindex_value(const char *name, unsigned int *lenname);

/* compare an entry with a name, outcome: -1:0:1 if entry is less, equal or bigger
*/

int compare_entry(struct notifyfshm_entry_struct *entry, const char *name, unsigned int nameindex_value, int len0);

/*
    get the directory for a certain entry
    if create==1, then when directory does not exist yet for parent, it's created
*/

struct notifyfshm_directory_struct *get_directory_sl(struct notifyfshm_entry_struct *parent, unsigned char create, unsigned int *error);

#endif
