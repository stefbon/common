/*
 
  2010, 2011 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#define LOG_LOGAREA LOG_LOGAREA_SOCKET

#include "logging.h"
#include "epoll-utils.h"
#include "socket.h"

#define LISTEN_BACKLOG 50

void init_connection(struct notifyfs_connection_struct *connection)
{

    connection->fd=0;
    connection->typedata=0;
    connection->type=0;
    connection->allocated=0;
    connection->data=NULL;
    connection->process_event=NULL;

    init_xdata(&connection->xdata_socket);

}

int process_socket_event(int fd, void *data, uint32_t events)
{

    if ( events & ( EPOLLHUP | EPOLLRDHUP ) ) {
	struct notifyfs_connection_struct *notifyfs_connection=(struct notifyfs_connection_struct *) data;

	close(fd);

	remove_xdata_from_epoll(&notifyfs_connection->xdata_socket);
	remove_xdata_from_list(&notifyfs_connection->xdata_socket, 0);

	/* callback for the client connections */

	if (notifyfs_connection->process_event) {

	    int res=(* notifyfs_connection->process_event) (notifyfs_connection, events);

	}

    } else {
	struct notifyfs_connection_struct *notifyfs_connection=(struct notifyfs_connection_struct *) data;

        /* here handle the data available for process */

        logoutput("process_socket_event: handle event %i on fd %i", events, fd);

	if (notifyfs_connection->process_event) {

	    int res=(* notifyfs_connection->process_event) (notifyfs_connection, events);

	}

    }

    return 0;

}


static int accept_connection_client(struct notifyfs_connection_struct *server_connection, struct notifyfs_connection_struct *client_connection)
{
    int nreturn=0;

    if (server_connection->type==NOTIFYFS_CONNECTION_TYPE_INETSERVER) {
	socklen_t address_length=sizeof(struct sockaddr_in);

	client_connection->type=NOTIFYFS_CONNECTION_TYPE_INETCLIENT;

	client_connection->fd=accept4(server_connection->fd, (struct sockaddr *) &client_connection->socket.inet, &address_length, SOCK_NONBLOCK);

    } else if (server_connection->type==NOTIFYFS_CONNECTION_TYPE_LOCALSERVER) {
	socklen_t address_length=sizeof(struct sockaddr_un);

	client_connection->type=NOTIFYFS_CONNECTION_TYPE_LOCALCLIENT;

	client_connection->fd=accept4(server_connection->fd, (struct sockaddr *) &client_connection->socket.local, &address_length, SOCK_NONBLOCK);

    }


    if (client_connection->fd>0) {
	struct epoll_extended_data_struct *epoll_xdata;

	/* use the same eventloop for the new connection as the server socket */

	client_connection->xdata_socket.eventloop=server_connection->xdata_socket.eventloop;

	if (server_connection->process_event) {

	    logoutput("accept_connection: fd: %i", client_connection->fd);

	    /* connect_socket callback may set the eventloop, disconnect and proces_event callbacks */

	    int res=(* server_connection->process_event) (client_connection, 0);

	    if (res<0) {

		close(client_connection->fd);
		client_connection->fd=0;
		nreturn=-EACCES;
		goto out;

	    }

	}

	epoll_xdata=add_to_epoll(client_connection->fd, EPOLLIN | EPOLLET, process_socket_event, (void *) client_connection, &client_connection->xdata_socket, client_connection->xdata_socket.eventloop);

	if ( ! epoll_xdata ) {

	    logoutput("error adding client fd %i to mainloop", client_connection->fd);

	    nreturn=-EIO;
	    close(client_connection->fd);
	    client_connection->fd=0;

	} else {

	    add_xdata_to_list(epoll_xdata);

	}

    } else {

	nreturn=-errno;
	logoutput("accept_connection: error %i connecting socket", errno);

    }

    out:

    return nreturn;

}


/* act on action on server socket 
    this means that a client wants to connect
*/

static void accept_connection(int fd, void *data, uint32_t events)
{
    struct notifyfs_connection_struct *server_connection=(struct notifyfs_connection_struct *) data;
    struct notifyfs_connection_struct *client_connection=NULL;

    logoutput("accept_connection");

    client_connection=malloc(sizeof(struct notifyfs_connection_struct));

    if (client_connection) {

	client_connection->fd=0;
	client_connection->data=NULL;
	client_connection->process_event=NULL;

	init_xdata(&client_connection->xdata_socket);

	int res=accept_connection_client(server_connection, client_connection);

	if (res<0) {

	    logoutput("accept_connection: error %i", res);

	    /* free connection 
	     and close (fd)
	    */

	}

    } else {

	logoutput("accept_connection: error allocating memory for client connection");

    }

}

int create_local_serversocket(char *path, struct notifyfs_connection_struct *server_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *connection, uint32_t events))
{
    int nreturn=0;

    if (! server_connection) {

	nreturn=-EINVAL;
	goto out;

    }

    server_connection->type=NOTIFYFS_CONNECTION_TYPE_LOCALSERVER;

    /* add socket */

    server_connection->fd=socket(PF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);

    if ( server_connection->fd < 0 ) {

        nreturn=-errno;
        goto out;

    }

    /* bind path/familiy and socket */

    memset(&server_connection->socket.local, 0, sizeof(struct sockaddr_un));

    server_connection->socket.local.sun_family=AF_UNIX;
    snprintf(server_connection->socket.local.sun_path, sizeof(server_connection->socket.local.sun_path), path);

    if ( bind(server_connection->fd, (struct sockaddr *) &server_connection->socket.local, sizeof(struct sockaddr_un)) != 0) {

        nreturn=-errno;
	close(server_connection->fd);
	server_connection->fd=0;
        goto out;

    }

    /* listen */

    if ( listen(server_connection->fd, LISTEN_BACKLOG) != 0 ) {

        nreturn=-errno;
	close(server_connection->fd);
	server_connection->fd=0;
        goto out;

    } else {
	struct epoll_extended_data_struct *epoll_xdata;

	epoll_xdata=add_to_epoll(server_connection->fd, EPOLLIN, accept_connection, (void *) server_connection, &server_connection->xdata_socket, eventloop);

	if ( ! epoll_xdata ) {

    	    logoutput("create_server_socket: error adding socket fd %i to eventloop.", server_connection->fd);

	    nreturn=-errno;
	    close(server_connection->fd);
	    server_connection->fd=0;

	} else {

    	    logoutput("create_server_socket: socket fd %i added to eventloop", server_connection->fd);

	    add_xdata_to_list(epoll_xdata);
	    nreturn=server_connection->fd;
	    server_connection->process_event=callback;

	}

    }

    out:

    return nreturn;

}

int create_inet_serversocket(int family, int port, struct notifyfs_connection_struct *server_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *connection, uint32_t events))
{
    int nreturn=0, res;

    if (! server_connection) {

	nreturn=-EINVAL;
	goto out;

    } else if (!(family==AF_INET || family==AF_INET6)) {

	nreturn=-EINVAL;
	goto out;

    }

    /* add socket */

    if (family==AF_INET) {

	server_connection->fd=socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);

    } else if (family==AF_INET6) {

	server_connection->fd=socket(PF_INET6, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);

    }

    if ( server_connection->fd < 0 ) {

        nreturn=-errno;
        goto out;

    }

    if (family==AF_INET) {

	memset(&server_connection->socket.inet, 0, sizeof(struct sockaddr_in));

	server_connection->type=NOTIFYFS_CONNECTION_TYPE_INETSERVER;

	server_connection->socket.inet.sin_family=AF_INET;
	server_connection->socket.inet.sin_addr.s_addr=INADDR_ANY;
	server_connection->socket.inet.sin_port=htons(port);

	res=bind(server_connection->fd, (struct sockaddr *) &server_connection->socket.inet, sizeof(struct sockaddr_in));

    } else if (family==AF_INET6) {

	memset(&server_connection->socket.inet, 0, sizeof(struct sockaddr_in));

	server_connection->type=NOTIFYFS_CONNECTION_TYPE_INET6SERVER;

	server_connection->socket.inet6.sin6_family=AF_INET6;
	server_connection->socket.inet6.sin6_addr=in6addr_any;
	server_connection->socket.inet6.sin6_port=htons(port);

	res=bind(server_connection->fd, (struct sockaddr *) &server_connection->socket.inet6, sizeof(struct sockaddr_in6));

    }

    if (res==-1) {

	/* bind failed */

        nreturn=-errno;
	close(server_connection->fd);
	server_connection->fd=0;
        goto out;

    }

    /* listen */

    if ( listen(server_connection->fd, LISTEN_BACKLOG) != 0 ) {

        nreturn=-errno;
	close(server_connection->fd);
	server_connection->fd=0;
        goto out;

    } else {
	struct epoll_extended_data_struct *epoll_xdata;

	epoll_xdata=add_to_epoll(server_connection->fd, EPOLLIN, accept_connection, (void *) server_connection, &server_connection->xdata_socket, eventloop);

	if ( ! epoll_xdata ) {

    	    logoutput("create_server_socket: error adding socket fd %i to eventloop.", server_connection->fd);

	    nreturn=-errno;
	    close(server_connection->fd);
	    server_connection->fd=0;

	} else {

    	    logoutput("create_server_socket: socket fd %i added to eventloop", server_connection->fd);

	    add_xdata_to_list(epoll_xdata);
	    nreturn=server_connection->fd;
	    server_connection->process_event=callback;

	}

    }

    out:

    return nreturn;

}


int create_local_clientsocket(char *path, struct notifyfs_connection_struct *client_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *client_connection, uint32_t events))
{
    int nreturn=0;

    logoutput("create_local_clientsocket");

    if (! client_connection) {

	nreturn=-EINVAL;
	goto out;

    }

    client_connection->type=NOTIFYFS_CONNECTION_TYPE_LOCALCLIENT;

    client_connection->fd=socket(PF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);

    if (client_connection->fd<0) {

	nreturn=-errno;
	goto out;

    }

    /* start with a clean address structure */

    memset(&client_connection->socket.local, 0, sizeof(struct sockaddr_un));

    client_connection->socket.local.sun_family = AF_UNIX;
    snprintf(client_connection->socket.local.sun_path, sizeof(client_connection->socket.local.sun_path), path);

    if ( connect(client_connection->fd, (struct sockaddr *) &client_connection->socket.local, sizeof(struct sockaddr_un)) == -1) {

	close(client_connection->fd);
	client_connection->fd=0;
    	nreturn=-errno;
	goto out;

    } else {
	struct epoll_extended_data_struct *epoll_xdata;

	/* add to eventloop */

	epoll_xdata=add_to_epoll(client_connection->fd, EPOLLIN, process_socket_event, (void *) client_connection, &client_connection->xdata_socket, eventloop);

	if ( ! epoll_xdata ) {

	    logoutput("initialize_socket: cannot add socket %i to eventloop", client_connection->fd);

	    close(client_connection->fd);
	    client_connection->fd=0;
	    nreturn=-EIO;

	} else {

	    logoutput("initialize_socket: added socket %i to eventloop", client_connection->fd);

	    add_xdata_to_list(epoll_xdata);
	    client_connection->process_event=callback;
	    nreturn=client_connection->fd;

	}

    }

    out:

    return nreturn;

}

int create_inet_clientsocket(int family, char *ipaddress, int port, struct notifyfs_connection_struct *client_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *client_connection, uint32_t events))
{
    int nreturn=0, res=-1;

    logoutput("create_inet_clientsocket");

    if (! client_connection) {

	nreturn=-EINVAL;
	goto out;

    } else if (!(family==AF_INET || family==AF_INET6)) {

	nreturn=-EINVAL;
	goto out;

    }

    if (family==AF_INET) {

	client_connection->fd=socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    } else if (family==AF_INET6) {

	client_connection->fd=socket(PF_INET6, SOCK_STREAM, IPPROTO_TCP);

    }

    if (client_connection->fd<0) {

	nreturn=-errno;
	goto out;

    }

    if (family==AF_INET) {

	memset(&client_connection->socket.inet, 0, sizeof(struct sockaddr_in));

	client_connection->type=NOTIFYFS_CONNECTION_TYPE_INETCLIENT;

	client_connection->socket.inet.sin_port=htons(port);
	client_connection->socket.inet.sin_family = AF_INET;

	inet_pton(AF_INET, (const char *) ipaddress, (void *) &client_connection->socket.inet.sin_addr.s_addr);

	res=connect(client_connection->fd, (struct sockaddr *) &client_connection->socket.inet, sizeof(struct sockaddr_in));

	if (res==-1) nreturn=-errno;

    } else if (family==AF_INET6) {

	memset(&client_connection->socket.inet6, 0, sizeof(struct sockaddr_in6));

	client_connection->type=NOTIFYFS_CONNECTION_TYPE_INET6CLIENT;

	client_connection->socket.inet6.sin6_port=htons(port);
	client_connection->socket.inet6.sin6_family = AF_INET6;

	inet_pton(AF_INET6, (const char *) ipaddress, (void *) &client_connection->socket.inet6.sin6_addr.s6_addr);

	res=connect(client_connection->fd, (struct sockaddr *) &client_connection->socket.inet6, sizeof(struct sockaddr_in6));

	if (res==-1) nreturn=-errno;

    } else {

	res=-1;
	nreturn=-EINVAL;

    }

    if ( res==-1) {

	close(client_connection->fd);
	client_connection->fd=0;
	goto out;

    } else {
	struct epoll_extended_data_struct *epoll_xdata;

	/* add to eventloop */

	epoll_xdata=add_to_epoll(client_connection->fd, EPOLLIN, process_socket_event, (void *) client_connection , &client_connection->xdata_socket, eventloop);

	if ( ! epoll_xdata ) {

	    logoutput("initialize_socket: cannot add socket %i to eventloop", client_connection->fd);

	    close(client_connection->fd);
	    client_connection->fd=0;
	    nreturn=-EIO;

	} else {

	    logoutput("initialize_socket: added socket %i to eventloop", client_connection->fd);

	    add_xdata_to_list(epoll_xdata);
	    client_connection->process_event=callback;
	    nreturn=client_connection->fd;

	}

    }

    out:

    return nreturn;

}

unsigned char is_remote(struct notifyfs_connection_struct *connection)
{

    return (connection->type==NOTIFYFS_CONNECTION_TYPE_INETCLIENT || connection->type==NOTIFYFS_CONNECTION_TYPE_INET6CLIENT) ? 1: 0;

}

unsigned char is_ipv4(struct notifyfs_connection_struct *connection)
{

    return (connection->type==NOTIFYFS_CONNECTION_TYPE_INETCLIENT) ? 1: 0;

}

unsigned char is_ipv6(struct notifyfs_connection_struct *connection)
{

    return (connection->type==NOTIFYFS_CONNECTION_TYPE_INET6CLIENT) ? 1: 0;

}
