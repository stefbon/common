/*
  2010, 2011 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef NOTIFYFS_SOCKET_H
#define NOTIFYFS_SOCKET_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <linux/netlink.h>

#define NOTIFYFS_CONNECTION_TYPE_INETSERVER					1
#define NOTIFYFS_CONNECTION_TYPE_INETCLIENT					2
#define NOTIFYFS_CONNECTION_TYPE_INET6SERVER					3
#define NOTIFYFS_CONNECTION_TYPE_INET6CLIENT					4
#define NOTIFYFS_CONNECTION_TYPE_LOCALSERVER					5
#define NOTIFYFS_CONNECTION_TYPE_LOCALCLIENT					6
#define NOTIFYFS_CONNECTION_TYPE_NETLINK					7

struct notifyfs_connection_struct {
    int fd;
    struct epoll_extended_data_struct xdata_socket;
    unsigned char typedata;
    void *data;
    union {
	struct sockaddr_un local;
	struct sockaddr_in inet;
	struct sockaddr_in6 inet6;
	struct sockaddr_nl netlink;
    } socket;
    unsigned char type;
    unsigned char allocated;
    int (*process_event) (struct notifyfs_connection_struct *notifyfs_connection, uint32_t events);
};

/* Prototypes */

void init_connection(struct notifyfs_connection_struct *connection);

int create_local_serversocket(char *path, struct notifyfs_connection_struct *server_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *connection, uint32_t events));
int create_inet_serversocket(int family, int port, struct notifyfs_connection_struct *server_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *connection, uint32_t events));

int create_local_clientsocket(char *path, struct notifyfs_connection_struct *client_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *client_connection, uint32_t events));
int create_inet_clientsocket(int family, char *ipaddres, int port, struct notifyfs_connection_struct *client_connection, struct epoll_eventloop_struct *eventloop, int (*callback) (struct notifyfs_connection_struct *client_connection, uint32_t events));

unsigned char is_remote(struct notifyfs_connection_struct *connection);
unsigned char is_ipv4(struct notifyfs_connection_struct *connection);
unsigned char is_ipv6(struct notifyfs_connection_struct *connection);

#endif



